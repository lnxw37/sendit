<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class Fluidframe {
	
	protected static $is_api;
	protected static $have_config;
	protected static $plugins = array();
	
    public function haveConfig() {
        return self::$have_config;
    }
	public function isApi() {
        return self::$is_api;
    }

    public function setApi($mode) {
        self::$is_api = $mode;
    }
	
    public static function addPlugin($name, $attrs = null)
    {
        $name = ucfirst($name);
        $pluginclass = "{$name}Plugin";

        if (!class_exists($pluginclass)) {

            $files = array("local/plugins/{$pluginclass}.php",
                           "local/plugins/{$name}/{$pluginclass}.php",
                           "local/{$pluginclass}.php",
                           "local/{$name}/{$pluginclass}.php",
                           "plugins/{$pluginclass}.php",
                           "plugins/{$name}/{$pluginclass}.php");

            foreach ($files as $file) {
                $fullpath = INSTALLDIR.'/'.$file;
                if (@file_exists($fullpath)) {
                    include_once($fullpath);
                    break;
                }
            }
            if (!class_exists($pluginclass)) {
                throw new ServerException("Plugin $name not found.", 500);
            }
        }

        $inst = new $pluginclass();
        if (!empty($attrs)) {
            foreach ($attrs as $aname => $avalue) {
                $inst->$aname = $avalue;
            }
        }

        // Record activated plugins for later display/config dump
        self::$plugins[] = array($name, $attrs);

        return true;
    }
    
    public static function init($server=null, $path=null, $conffile=null) {
        Fluidframe::initDefaults($server, $path);
        Fluidframe::loadConfigFile($conffile);
        
        self::initPlugins();
    }
    
    protected static function defaultConfig() {
        global $_server, $_path;
        require(INSTALLDIR.'/lib/default.php');
        return $default;
    }
    
	    protected static function initDefaults($server, $path)
    {
        global $_server, $_path, $config;

        Event::clearHandlers();
        self::$plugins = array();

        // try to figure out where we are. $server and $path
        // can be set by including module, else we guess based
        // on HTTP info.

        if (isset($server)) {
            $_server = $server;
        } else {
            $_server = array_key_exists('SERVER_NAME', $_SERVER) ?
              strtolower($_SERVER['SERVER_NAME']) :
            null;
        }

        if (isset($path)) {
            $_path = $path;
        } else {
            $_path = (array_key_exists('SERVER_NAME', $_SERVER) && array_key_exists('SCRIPT_NAME', $_SERVER)) ?
              self::_sn_to_path($_SERVER['SCRIPT_NAME']) :
            null;
        }

        // Set config values initially to default values
        $default = self::defaultConfig();
        $config = $default;

        // Backward compatibility

        $config['site']['design'] =& $config['design'];

        if (function_exists('date_default_timezone_set')) {
            /* Work internally in UTC */
            date_default_timezone_set('UTC');
        }
    }

    protected function _sn_to_path($sn)
    {
        $past_root = substr($sn, 1);
        $last_slash = strrpos($past_root, '/');
        if ($last_slash > 0) {
            $p = substr($past_root, 0, $last_slash);
        } else {
            $p = '';
        }
        return $p;
    }

    /**
     * Load the default or specified configuration file.
     * Modifies global $config and may establish plugins.
     *
     * @throws NoConfigException
     */
    protected function loadConfigFile($conffile=null)
    {
        global $_server, $_path, $config;

        // From most general to most specific:
        // server-wide, then vhost-wide, then for a path,
        // finally for a dir (usually only need one of the last two).

        if (isset($conffile)) {
            $config_files = array($conffile);
        } else {
            $config_files = array(INSTALLDIR.'/config.php');
        }

        self::$have_config = false;

        foreach ($config_files as $_config_file) {
            if (@file_exists($_config_file)) {
                // Ignore 0-byte config files
                if (filesize($_config_file) > 0) {
                    include($_config_file);
                    self::$have_config = true;
                }
            }
        }

        if (!self::$have_config) {
            throw new NoConfigException("No configuration file found.",
                                        $config_files);
        }

    }

    /**
     * Are we running from the web with HTTPS?
     *
     * @return boolean true if we're running with HTTPS; else false
     */

    static function isHTTPS()
    {
        // There are some exceptions to this; add them here!
        if(empty($_SERVER['HTTPS'])) {
            return false;
        } else {
            return $_SERVER['HTTPS'] !== 'off';
        }
    }
    
    protected static function initPlugins() {
        // Load default plugins
        foreach (common_config('plugins', 'default') as $name => $params) {
            if (is_null($params)) {
                addPlugin($name);
            } else if (is_array($params)) {
                if (count($params) == 0) {
                    addPlugin($name);
                } else {
                    $keys = array_keys($params);
                    if (is_string($keys[0])) {
                        addPlugin($name, $params);
                    } else {
                        foreach ($params as $paramset) {
                            addPlugin($name, $paramset);
                        }
                    }
                }
            }
        }

        // XXX: if plugins should check the schema at runtime, do that here.
        if (common_config('db', 'schemacheck') == 'runtime') {
            Event::handle('CheckSchema');
        }

        // Give plugins a chance to initialize in a fully-prepared environment
        Event::handle('InitializePlugin');
    }
    
}

class NoConfigException extends Exception
{
    public $configFiles;

    function __construct($msg, $configFiles) {
        parent::__construct($msg);
        $this->configFiles = $configFiles;
    }
}
