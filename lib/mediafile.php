<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class MediaFile {


    static function fromUpload($filename, $param = 'file')  {
        if (!isset($_FILES[$param]['error'])){
        	common_debug('File ' . $param . ' non trovato');
        	throw new ClientException(_('File _FILE[' . $param . '] not found'));
        }

        switch ($_FILES[$param]['error']) {
        case UPLOAD_ERR_OK: // success, jump out
            break;
        case UPLOAD_ERR_INI_SIZE:
            // TRANS: Client exception thrown when an uploaded file is larger than set in php.ini.
            throw new ClientException(_('The uploaded file exceeds the ' .
                'upload_max_filesize directive in php.ini.'));
            return;
        case UPLOAD_ERR_FORM_SIZE:
            throw new ClientException(
                // TRANS: Client exception.
                _('The uploaded file exceeds the MAX_FILE_SIZE directive' .
                ' that was specified in the HTML form.'));
            return;
        case UPLOAD_ERR_PARTIAL:
            @unlink($_FILES[$param]['tmp_name']);
            // TRANS: Client exception.
            throw new ClientException(_('The uploaded file was only' .
                ' partially uploaded.'));
            return;
        case UPLOAD_ERR_NO_FILE:
            // No file; probably just a non-AJAX submission.
            return;
        case UPLOAD_ERR_NO_TMP_DIR:
            // TRANS: Client exception thrown when a temporary folder is not present to store a file upload.
            throw new ClientException(_('Missing a temporary folder.'));
            return;
        case UPLOAD_ERR_CANT_WRITE:
            // TRANS: Client exception thrown when writing to disk is not possible during a file upload operation.
            throw new ClientException(_('Failed to write file to disk.'));
            return;
        case UPLOAD_ERR_EXTENSION:
            // TRANS: Client exception thrown when a file upload operation has been stopped by an extension.
            throw new ClientException(_('File upload stopped by extension.'));
            return;
        default:
            common_log(LOG_ERR, __METHOD__ . ": Unknown upload error " .
                $_FILES[$param]['error']);
            // TRANS: Client exception thrown when a file upload operation has failed with an unknown reason.
            throw new ClientException(_('System error uploading file.'));
            return;
        }

        $filedir = common_config('site','upd-path');
        if(!file_exists($filedir))
        	mkdir($filedir);
        $filepath = $filedir . '/' . $filename;
        $result = move_uploaded_file($_FILES[$param]['tmp_name'], $filepath);

        if (!$result) {
        	// TRANS: Client exception thrown when a file upload operation fails because the file could
        	// TRANS: not be moved from the temporary folder to the permanent file location.
        	throw new ClientException(_('File could not be moved to destination directory.'));
        	return;
        }
        common_debug('File moved: from ' . $_FILES[$param]['tmp_name'] . ' to ' . $filepath);
		file_put_contents($filepath.'.info', $_FILES[$param]['name']);
        return true;
    }
}