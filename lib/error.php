<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

require_once INSTALLDIR . '/lib/info.php';

/**
 * Base class for displaying HTTP errors
 *
 * @category Action
 * @package  Fluidframe
 * @author   Zach Copley <zach@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 * @link     http://status.net/
 */
class ErrorAction extends InfoAction
{
    static $status = array();

    var $code    = null;
    var $message = null;
    var $default = null;

    function __construct($message, $code, $output='php://output', $indent=null)
    {
        parent::__construct(null, $message, $output, $indent);

        $this->code = $code;
        $this->message = $message;
        $this->minimal = Fluidframe::isApi();

        // XXX: hack alert: usually we aren't going to
        // call this page directly, but because it's
        // an action it needs an args array anyway
        $this->prepare($_REQUEST);
    }

    function showPage()
    {
        if ($this->minimal) {
            // Even more minimal -- we're in a machine API
            // and don't want to flood the output.
            $this->extraHeaders();
            $this->showContent();
        } else {
            parent::showPage();
        }

        // We don't want to have any more output after this
        exit();
    }

    /**
     * Display content.
     *
     * @return nothing
     */
    function showContent() {
    	$this->elementStart('div',array('id'=>'main-home'));
    	$this->element('div', array('class' => 'error'), $this->message);
    	$this->elementEnd('div');
    }



}
