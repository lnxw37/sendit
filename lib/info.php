<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

/**
 * Base class for displaying dialog box like messages to the user
 *
 * @category Action
 * @package  Fluidframe
 * @author   Zach Copley <zach@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 * @link     http://status.net/
 *
 * @see ErrorAction
 */

class InfoAction extends FluidFrameAction {
    var $message = null;

    function __construct($title, $message, $output='php://output', $indent=null)
    {
        parent::__construct($output, $indent);

        $this->message = $message;
        $this->title   = $title;

        // XXX: hack alert: usually we aren't going to
        // call this page directly, but because it's
        // an action it needs an args array anyway
        $this->prepare($_REQUEST);
    }
    
    /**
     * Page title.
     *
     * @return page title
     */

    function title()
    {
        return empty($this->title) ? '' : $this->title;
    }

    function isReadOnly($args)
    {
        return true;
    }

    // Overload a bunch of stuff so the page isn't too bloated

//     function showBody()
//     {
//         $this->elementStart('body', array('id' => 'error'));
//         $this->elementStart('div', array('id' => 'wrap'));
//         $this->showHeader();
//         $this->showCore();
//         $this->showFooter();
//         $this->elementEnd('div');
//         $this->elementEnd('body');
//     }

//     function showCore()
//     {
//         $this->elementStart('div', array('id' => 'core'));
//         $this->showContentBlock();
//         $this->elementEnd('div');
//     }

//     function showHeader()
//     {
//         $this->elementStart('div', array('id' => 'header'));
//         $this->showLogo();
//         $this->showPrimaryNav();
//         $this->showTopMessage();
//         $this->elementEnd('div');
//     }

    /**
     * Display content.
     *
     * @return nothing
     */
    function showContent()
    {
    	$this->elementStart('div',array('id'=>'main-home'));
    	$this->element('div', array('class' => 'info'), $this->message);
    	$this->elementEnd('div');
    	
        
    }

}
