<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

/**
 * Base class for plugins
 *
 * Subclasses of Plugin will automatically handle an event if they define
 * a method called "onEventName". (Well, OK -- only if they call parent::__construct()
 * in their constructors.)
 *
 * They will also automatically handle the InitializePlugin and CleanupPlugin with the
 * initialize() and cleanup() methods, respectively.
 *
 */

class Plugin
{
    function __construct()
    {
        Event::addHandler('InitializePlugin', array($this, 'initialize'));
        Event::addHandler('CleanupPlugin', array($this, 'cleanup'));

        foreach (get_class_methods($this) as $method) {
            if (mb_substr($method, 0, 2) == 'on') {
                Event::addHandler(mb_substr($method, 2), array($this, $method));
            }
        }

        $this->setupGettext();
    }

    function initialize()
    {
        return true;
    }

    function cleanup()
    {
        return true;
    }

    /**
     * Checks if this plugin has localization that needs to be set up.
     * Gettext localizations can be called via the _m() helper function.
     */
    protected function setupGettext()
    {
        $class = get_class($this);
        if (substr($class, -6) == 'Plugin') {
            $name = substr($class, 0, -6);
            $path = common_config('plugins', 'locale_path');
            if (!$path) {
                // @fixme this will fail for things installed in local/plugins
                // ... but then so will web links so far.
                $path = INSTALLDIR . "/plugins/$name/locale";
            }
            if (file_exists($path) && is_dir($path)) {
                bindtextdomain($name, $path);
                bind_textdomain_codeset($name, 'UTF-8');
            }
        }
    }

    protected function log($level, $msg)
    {
        common_log($level, get_class($this) . ': '.$msg);
    }

    protected function debug($msg)
    {
        $this->log(LOG_DEBUG, $msg);
    }
    
    function name()
    {
        $cls = get_class($this);
        return mb_substr($cls, 0, -6);
    }

    function onPluginVersion(&$versions)
    {
        $name = $this->name();

        $versions[] = array('name' => $name,
                            // TRANS: Displayed as version information for a plugin if no version information was found.
                            'version' => _('Unknown'));

        return true;
    }

    function path($relative)
    {
        return self::staticPath($this->name(), $relative);
    }

    static function staticPath($plugin, $relative)
    {
        $isHTTPS = Fluidframe::isHTTPS();

        if ($isHTTPS) {
            $server = common_config('plugins', 'sslserver');
        } else {
            $server = common_config('plugins', 'server');
        }

        if (empty($server)) {
            if ($isHTTPS) {
                $server = common_config('site', 'sslserver');
            }
            if (empty($server)) {
                $server = common_config('site', 'server');
            }
        }

        if ($isHTTPS) {
            $path = common_config('plugins', 'sslpath');
        } else {
            $path = common_config('plugins', 'path');
        }

        if (empty($path)) {
            $path = common_config('site', 'path') . '/plugins/';
        }

        if ($path[strlen($path)-1] != '/') {
            $path .= '/';
        }

        if ($path[0] != '/') {
            $path = '/'.$path;
        }

        $protocol = ($isHTTPS) ? 'https' : 'http';

        return $protocol.'://'.$server.$path.$plugin.'/'.$relative;
    }
}
