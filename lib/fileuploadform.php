<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011-2012, Fluidware srl
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class FileUploadForm extends Form {

	var $ukey;

	function __construct($out=null, $ukey=false) {
		parent::__construct($out);
		$this->ukey = $ukey;
	}
	
    /**
     * ID of the form
     *
     * @return string ID of the form
     */
    function id() {
    	return 'form_file_upload_add';
    }

    /**
     * HTTP method used to submit the form
     *
     * For image data we need to send multipart/form-data
     * so we set that here too
     *
     * @return string the method to use for submitting
     */
    function method() {
        $this->enctype = 'multipart/form-data';
        return 'post';
    }

    /**
     * class of the form
     *
     * @return string of the form class
     */
    function formClass() {
        return 'form_settings';
    }

    /**
     * Action of the form
     *
     * @return string URL of the action
     */
    function action() {
       return common_local_url('upload',null,array('X-Progress-ID'=>$this->ukey));
    }

    /**
     * Data elements of the form
     *
     * @return void
     */
    function formData()    {
    	
    	
    	$this->out->hidden('APC_UPLOAD_PROGRESS',$this->ukey);

    	$this->out->elementStart('div');
    	$this->out->input('email', _('Recipient Email'),'');
    	$this->out->elementEnd('div');
    	
    	$max_filesize = ImageFile::maxFileSizeInt();
    	$this->out->element('input', 
    		array('name' => 'MAX_FILE_SIZE',
	    		'type' => 'hidden',
	    	    'id' => 'MAX_FILE_SIZE',
	    	    'value' => $max_filesize));
    	        
        $this->out->element('br');
        
        $this->out->element('input', array('name' => 'filename',
        					'type' => 'file',
                            'id' => 'filename'));

        $this->out->element('div',null, sprintf(_('Max size %s bytes'),common_bytes_to_size($max_filesize)));

        
    }

    /**
     * Action elements
     *
     * @return void
     */
    function formActions() {
        
        // TRANS: Button label in the "Edit annuncio" form.
        $this->out->submit('save', _m('BUTTON','Send it!'), 'submit buttonmail',
                           // TRANS: Submit button title.
                           'save', _('Send It!'));
    }
}
