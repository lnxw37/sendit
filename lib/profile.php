<?php 

class Profile {
	
	var $id;
	var $username;
	var $email;
	var $fullname;
	var $language;
	
	static function staticGet($id) {
		
		$sjson = $_SESSION['profile'.$id];
		if(empty($sjson)) {
			return false;
		}
		$json = json_decode($sjson);
		$profile = new Profile();
		$profile->id = $id;
		$profile->username = $json->username;
		$profile->email = $json->email;
		$profile->fullname = $json->fullname;
		return $profile;
	}

	function hasRight($right) {
		return true;
	}
	
	function hasRole($role) {
		return true;
	}
	
	function toJsonString() {
		$json = array('id'=>$this->id,'email'=>$this->email,'fullname'=>$this->fullname,'username'=>$this->username);
		return json_encode($json);
	}
}

?>