<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class Action extends HTMLOutputter {
	
    var $args;

    /**
     * Constructor
     *
     * Just wraps the HTMLOutputter constructor.
     *
     * @param string  $output URI to output to, default = stdout
     * @param boolean $indent Whether to indent output, default true
     *
     * @see XMLOutputter::__construct
     * @see HTMLOutputter::__construct
     */
    function __construct($output='php://output', $indent=null)
    {
        parent::__construct($output, $indent);
    }

    /**
     * For initializing members of the class.
     *
     * @param array $argarray misc. arguments
     *
     * @return boolean true
     */
    function prepare($argarray)
    {
        $this->args =& common_copy_args($argarray);

        return true;
    }

    /**
     * Show page, a template method.
     *
     * @return nothing
     */
    function showPage()
    {
        if (Event::handle('StartShowHTML', array($this))) {
            $this->startHTML();
            Event::handle('EndShowHTML', array($this));
        }
        if (Event::handle('StartShowHead', array($this))) {
            $this->showHead();
            Event::handle('EndShowHead', array($this));
        }
        if (Event::handle('StartShowBody', array($this))) {
            $this->showBody();
            Event::handle('EndShowBody', array($this));
        }
        if (Event::handle('StartEndHTML', array($this))) {
            $this->endHTML();
            Event::handle('EndEndHTML', array($this));
        }
    }

    function endHTML()
    {
        global $_startTime;

        if (isset($_startTime)) {
            $endTime = microtime(true);
            $diff = round(($endTime - $_startTime) * 1000);
            $this->raw("<!-- ${diff}ms -->\n");
        }

        return parent::endHTML();
    }

    /**
     * Show head, a template method.
     *
     * @return nothing
     */
    function showHead()  {
        // XXX: attributes (profile?)
        $this->elementStart('head');
        $this->element('meta',array('name'=>'generator','content'=>'fluidframe'));
        $this->element('meta',array('name'=>'author','content'=>'Michele Azzolari, Fluidware srl - Fluidframe'));
        $this->element('meta', array('name'=>'viewport','content'=> 'width = 800'));
        if (Event::handle('StartShowHeadElements', array($this))) {
            if (Event::handle('StartShowHeadTitle', array($this))) {
                $this->showTitle();
                Event::handle('EndShowHeadTitle', array($this));
            }
            $this->showShortcutIcon();
            $this->showStylesheets();
            $this->showDescription();
            $this->extraHead();
            
            Event::handle('EndShowHeadElements', array($this));
        }
        $this->elementEnd('head');
    }

    /**
     * Show title, a template method.
     *
     * @return nothing
     */
    function showTitle()
    {
        $this->element('title', null,
                       // TRANS: Page title. %1$s is the title, %2$s is the site name.
                       sprintf(_("%1\$s - %2\$s"),
                               $this->title(),
                               common_config('site', 'name')));
        $this->element('meta', array('name'=>'title','content'=>
        // TRANS: Page title. %1$s is the title, %2$s is the site name.
        sprintf(_("%1\$s - %2\$s"),
        $this->title(),
        common_config('site', 'name'))));
        
    }

    /**
     * Returns the page title
     *
     * SHOULD overload
     *
     * @return string page title
     */

    function title() {
        // TRANS: Page title for a page without a title set.
        return _("Untitled page");
    }

    /**
     * Show themed shortcut icon
     *
     * @return nothing
     */
    function showShortcutIcon() {
    }

    /**
     * Show stylesheets
     *
     * @return nothing
     */
    function showStylesheets() {
        if (Event::handle('StartShowStyles', array($this))) {
        	$this->cssLink('theme/style.css',null,'screen, projection, tv, print');
        	Event::handle('EndShowStyle', array($this));
        }
    }

    /**
     * Show javascript headers
     *
     * @return nothing
     */
    function showScripts() {
        if (Event::handle('StartShowScripts', array($this))) {
        	$this->script('jquery.min.js');
        	$this->script('util.js');
            Event::handle('EndShowScripts', array($this));
        }
    }

    /**
     * Exports a map of localized text strings to JavaScript code.
     *
     * Plugins can add to what's exported by hooking the StartScriptMessages or EndScriptMessages
     * events and appending to the array. Try to avoid adding strings that won't be used, as
     * they'll be added to HTML output.
     */

    function showScriptMessages()  {
        $messages = array();

        if (Event::handle('StartScriptMessages', array($this, &$messages))) {
            //$messages['showmore_tooltip'] = _m('TOOLTIP', 'Show more');
            $messages = array_merge($messages, $this->getScriptMessages());
	    	Event::handle('EndScriptMessages', array($this, &$messages));
        }
        if (!empty($messages)) {
            $this->inlineScript('CL.messages=' . json_encode($messages));
        }
        return $messages;
    }

    /**
     * If the action will need localizable text strings, export them here like so:
     *
     * return array('pool_deepend' => _('Deep end'),
     *              'pool_shallow' => _('Shallow end'));
     *
     * The exported map will be available via SN.msg() to JS code:
     *
     *   $('#pool').html('<div class="deepend"></div><div class="shallow"></div>');
     *   $('#pool .deepend').text(SN.msg('pool_deepend'));
     *   $('#pool .shallow').text(SN.msg('pool_shallow'));
     *
     * Exports a map of localized text strings to JavaScript code.
     *
     * Plugins can add to what's exported on any action by hooking the StartScriptMessages or
     * EndScriptMessages events and appending to the array. Try to avoid adding strings that won't
     * be used, as they'll be added to HTML output.
     */
    function getScriptMessages()
    {
        return array();
    }

    /**
     * Show description.
     *
     * SHOULD overload
     *
     * @return nothing
     */
    function showDescription() {
        // does nothing by default
    }

    /**
     * Show extra stuff in <head>.
     *
     * MAY overload
     *
     * @return nothing
     */
    function extraHead() {
    }

    /**
     * Show body.
     *
     * Calls template methods
     *
     * @return nothing
     */
    function showBody() {
    	
        $this->elementStart('body');
        $this->elementStart('div', array('id' => 'wrap'));
        $this->elementStart('div', array('id' => 'core'));
        
        if (Event::handle('StartShowHeader', array($this))) {
            $this->showHeader();
            Event::handle('EndShowHeader', array($this));
        }
        $this->elementStart('div', array('id' => 'container'));
        $this->showCore();
        $this->elementEnd('div'); // container
        $this->showFooter();
        $this->elementEnd('div'); // core
        $this->elementEnd('div'); // wrap
        $this->showScripts();
        $this->showPageScripts();
        $this->elementEnd('body');
    }

    function showPageScripts() {
    }
    
    /**
     * Show header of the page.
     *
     * Calls template methods
     *
     * @return nothing
     */
    function showHeader() {
    	$this->elementStart('div', array('id' => 'header'));
    	$this->showLogo();
    	$this->showPrimaryNav();
        $this->elementEnd('div');
    }

    function showSiteTitle() {
    }
    
    /**
     * Show configured logo.
     *
     * @return nothing
     */
    function showLogo() {
    }

    /**
     * Show primary navigation.
     *
     * @return nothing
     */
    function showPrimaryNav() {
    }

    /**
     * Show core.
     *
     *
     * @return nothing
     */
    function showCore() {
        if (Event::handle('StartShowContentBlock', array($this))) {
            $this->showContentBlock();
            Event::handle('EndShowContentBlock', array($this));
        }
    }

    /**
     * Show local navigation block.
     *
     * @return nothing
     */
    function showLocalNavBlock() {
        $this->elementStart('dl', array('id' => 'site_nav_local_views'));
        // TRANS: DT element for local views block. String is hidden in default CSS.
        $this->element('dt', null, _('Local views'));
        $this->elementStart('dd');
        $this->showLocalNav();
        $this->elementEnd('dd');
        $this->elementEnd('dl');
    }

    /**
     * Show local navigation.
     *
     * SHOULD overload
     *
     * @return nothing
     */
    function showLocalNav()
    {
        // does nothing by default
    }
    
    /**
     * Show content block.
     *
     * @return nothing
     */
    function showContentBlock() {
        $this->showContent();
    }

    /**
     * Show page title.
     *
     * @return nothing
     */
    function showPageTitle()
    {
    }



    /**
     * Show page notice.
     *
     * SHOULD overload (unless there's not a notice)
     *
     * @return nothing
     */
    function showPageNotice()
    {
    }

    /**
     * Show content.
     *
     * MUST overload (unless there's not a notice)
     *
     * @return nothing
     */
    function showContent()
    {
    }

    /**
     * Show Aside.
     *
     * @return nothing
     */
    function showAside() {
    }

    /**
     * Show export data feeds.
     *
     * @return void
     */
    function showExportData()
    {
        $feeds = $this->getFeeds();
        if ($feeds) {
            $fl = new FeedList($this);
            $fl->show($feeds);
        }
    }

    /**
     * Show sections.
     *
     * SHOULD overload
     *
     * @return nothing
     */
    function showSections()
    {
        // for each section, show it
    }

    /**
     * Show footer.
     *
     * @return nothing
     */
    function showFooter()  {
    	$this->elementStart('div',array('id'=>'footer','class'=>'testo'));
		$this->element('span',null,'');
    	$this->elementEnd('div');
    }

    /**
    * Show secondary navigation upper.
    *
    * @return nothing
    */
    function showSecondaryUpperNav() {
    	
    }
    
    /**
     * Show secondary navigation bottom.
     *
     * @return nothing
     */
    function showSecondaryBottomNav() {
    	

    }

    /**
     * Show licenses.
     *
     * @return nothing
     */
    function showCopyright() {
    	
    	
        $this->elementStart('div', array('id'=>'footer_copyright'));
        
        $instr = ' &copy; 2012 <a href="http://www.fluidware.it" title="Flu:idWeb">%%site.copyright%%</a>';

        $output = common_markup_to_html($instr);
        $this->raw($output);
        $this->elementEnd('div');
    }
    
    /**
     * Return last modified, if applicable.
     *
     * MAY override
     *
     * @return string last modified http header
     */
    function lastModified()
    {
        // For comparison with If-Last-Modified
        // If not applicable, return null
        return null;
    }

    /**
     * Return etag, if applicable.
     *
     * MAY override
     *
     * @return string etag http header
     */
    function etag()
    {
        return null;
    }

    /**
     * Return true if read only.
     *
     * MAY override
     *
     * @param array $args other arguments
     *
     * @return boolean is read only action?
     */
    function isReadOnly($args)
    {
        return false;
    }

    /**
     * Returns query argument or default value if not found
     *
     * @param string $key requested argument
     * @param string $def default value to return if $key is not provided
     *
     * @return boolean is read only action?
     */
    function arg($key, $def=null)
    {
        if (array_key_exists($key, $this->args)) {
            return $this->args[$key];
        } else {
            return $def;
        }
    }

    /**
     * Returns trimmed query argument or default value if not found
     *
     * @param string $key requested argument
     * @param string $def default value to return if $key is not provided
     *
     * @return boolean is read only action?
     */
    function trimmed($key, $def=null)
    {
        $arg = $this->arg($key, $def);
        return is_string($arg) ? trim($arg) : $arg;
    }

    /**
     * Handler method
     *
     * @param array $argarray is ignored since it's now passed in in prepare()
     *
     * @return boolean is read only action?
     */
    function handle($argarray=null)
    {
        header('Vary: Accept-Encoding,Cookie');

        $lm   = $this->lastModified();
        $etag = $this->etag();

        if ($etag) {
            header('ETag: ' . $etag);
        }

        if ($lm) {
            header('Last-Modified: ' . date(DATE_RFC1123, $lm));
            if ($this->isCacheable()) {
                header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', 0 ) . ' GMT' );
                header( "Cache-Control: private, must-revalidate, max-age=0" );
                header( "Pragma:");
            }
        }

        $checked = false;
        if ($etag) {
            $if_none_match = (array_key_exists('HTTP_IF_NONE_MATCH', $_SERVER)) ?
              $_SERVER['HTTP_IF_NONE_MATCH'] : null;
            if ($if_none_match) {
                // If this check fails, ignore the if-modified-since below.
                $checked = true;
                if ($this->_hasEtag($etag, $if_none_match)) {
                    header('HTTP/1.1 304 Not Modified');
                    // Better way to do this?
                    exit(0);
                }
            }
        }

        if (!$checked && $lm && array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER)) {
            $if_modified_since = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
            $ims = strtotime($if_modified_since);
            if ($lm <= $ims) {
                header('HTTP/1.1 304 Not Modified');
                // Better way to do this?
                exit(0);
            }
        }
    }

    /**
     * Is this action cacheable?
     *
     * If the action returns a last-modified
     *
     * @param array $argarray is ignored since it's now passed in in prepare()
     *
     * @return boolean is read only action?
     */
    function isCacheable()
    {
        return true;
    }

    /**
     * Has etag? (private)
     *
     * @param string $etag          etag http header
     * @param string $if_none_match ifNoneMatch http header
     *
     * @return boolean
     */
    function _hasEtag($etag, $if_none_match)
    {
        $etags = explode(',', $if_none_match);
        return in_array($etag, $etags) || in_array('*', $etags);
    }

    /**
     * Boolean understands english (yes, no, true, false)
     *
     * @param string $key query key we're interested in
     * @param string $def default value
     *
     * @return boolean interprets yes/no strings as boolean
     */
    function boolean($key, $def=false)
    {
        $arg = strtolower($this->trimmed($key));

        if (is_null($arg)) {
            return $def;
        } else if (in_array($arg, array('true', 'yes', '1', 'on'))) {
            return true;
        } else if (in_array($arg, array('false', 'no', '0'))) {
            return false;
        } else {
            return $def;
        }
    }

    /**
     * Integer value of an argument
     *
     * @param string $key      query key we're interested in
     * @param string $defValue optional default value (default null)
     * @param string $maxValue optional max value (default null)
     * @param string $minValue optional min value (default null)
     *
     * @return integer integer value
     */
    function int($key, $defValue=null, $maxValue=null, $minValue=null)
    {
        $arg = strtolower($this->trimmed($key));

        if (is_null($arg) || !is_integer($arg)) {
            return $defValue;
        }

        if (!is_null($maxValue)) {
            $arg = min($arg, $maxValue);
        }

        if (!is_null($minValue)) {
            $arg = max($arg, $minValue);
        }

        return $arg;
    }

    /**
     * Server error
     *
     * @param string  $msg  error message to display
     * @param integer $code http error code, 500 by default
     *
     * @return nothing
     */
    function serverError($msg, $code=500)
    {
        $action = $this->trimmed('action');
        common_debug("Server error '$code' on '$action': $msg", __FILE__);
        throw new ServerException($msg, $code);
    }

    /**
     * Client error
     *
     * @param string  $msg  error message to display
     * @param integer $code http error code, 400 by default
     *
     * @return nothing
     */
    function clientError($msg, $code=400)
    {
        $action = $this->trimmed('action');
        common_debug("User error '$code' on '$action': $msg", __FILE__);
        throw new ClientException($msg, $code);
    }

    /**
     * Returns the current URL
     *
     * @return string current URL
     */
    function selfUrl()
    {
        list($action, $args) = $this->returnToArgs();
        return common_local_url($action, $args);
    }

    /**
     * Returns arguments sufficient for re-constructing URL
     *
     * @return array two elements: action, other args
     */
    function returnToArgs()
    {
        $action = $this->trimmed('action');
        $args   = $this->args;
        unset($args['action']);
        if (common_config('site', 'fancy')) {
            unset($args['p']);
        }
        if (array_key_exists('submit', $args)) {
            unset($args['submit']);
        }
        foreach (array_keys($_COOKIE) as $cookie) {
            unset($args[$cookie]);
        }
        return array($action, $args);
    }

    /**
     * Generate a menu item
     *
     * @param string  $url         menu URL
     * @param string  $text        menu name
     * @param string  $title       title attribute, null by default
     * @param boolean $is_selected current menu item, false by default
     * @param string  $id          element id, null by default
     *
     * @return nothing
     */
    function menuItem($url, $text, $title=null, $is_selected=false, $id=null,$last = false)
    {
        // Added @id to li for some control.
        // XXX: We might want to move this to htmloutputter.php
        $lattrs = array();
        if ($is_selected) {
            $lattrs['class'] = 'current';
        }

        (is_null($id)) ? $lattrs : $lattrs['id'] = $id;

        $this->elementStart('li', $lattrs);
        $attrs['href'] = $url;
        if ($title) {
            $attrs['title'] = $title;
        }
        $this->element('a', $attrs, $text);
        $this->elementEnd('li');
        if(!$last) {
        	$this->elementStart('li');
        	$this->elementStart('a');
        	$this->raw('&#8226;');
        	$this->elementEnd('a');
        	$this->elementEnd('li');
        }
    }

    /**
     * Generate pagination links
     *
     * @param boolean $have_before is there something before?
     * @param boolean $have_after  is there something after?
     * @param integer $page        current page
     * @param string  $action      current action
     * @param array   $args        rest of query arguments
     *
     * @return nothing
     */
    // XXX: The messages in this pagination method only tailor to navigating
    //      notices. In other lists, "Previous"/"Next" type navigation is
    //      desirable, but not available.
    function pagination($have_before, $have_after, $page, $action, $args=null,$xargs=null)
    {
        // Does a little before-after block for next/prev page
        if ($have_before || $have_after) {
            $this->elementStart('dl', 'pagination');
            // TRANS: DT element for pagination (previous/next, etc.).
            $this->element('dt', null, _('Pagination'));
            $this->elementStart('dd', null);
            $this->elementStart('ul', array('class' => 'nav'));
        }
        if ($have_before) {
            $pargs   = array('page' => $page-1);
            if(!is_null($xargs))
            	$pargs = array_merge($pargs,$xargs);
            $this->elementStart('li', array('class' => 'nav_prev'));
            $this->elementStart('a', array('href' => common_local_url($action, $args, $pargs),
                                      'rel' => 'prev'));
            $this->element('img',array('alt'=>_('Successivo'),'src'=>'/images/freccia_prev.png'));
            $this->elementEnd('a');
            $this->elementEnd('li');
        }
        if ($have_after) {
            $pargs   = array('page' => $page+1);
            if(!is_null($xargs))
            	$pargs = array_merge($pargs,$xargs);
            $this->elementStart('li', array('class' => 'nav_next'));
            $this->elementStart('a', array('href' => common_local_url($action, $args, $pargs),
                                      'rel' => 'next'));
            $this->element('img',array('alt'=>_('Successivo'),'src'=>'/images/freccia_next.png'));
            $this->elementEnd('a');
            $this->elementEnd('li');
        }
        if ($have_before || $have_after) {
            $this->elementEnd('ul');
            $this->elementEnd('dd');
            $this->elementEnd('dl');
        }
    }

    /**
     * An array of feeds for this action.
     *
     * Returns an array of potential feeds for this action.
     *
     * @return array Feed object to show in head and links
     */
    function getFeeds()
    {
        return null;
    }

    /**
     * A design for this action
     *
     * @return Design a design object to use
     */
    function getDesign()
    {
        return null;
    }

    /**
     * Check the session token.
     *
     * Checks that the current form has the correct session token,
     * and throw an exception if it does not.
     *
     * @return void
     */
    // XXX: Finding this type of check with the same message about 50 times.
    //      Possible to refactor?
    function checkSessionToken()
    {
        // CSRF protection
        $token = $this->trimmed('token');
        if (empty($token) || $token != common_session_token()) {
            // TRANS: Client error text when there is a problem with the session token.
            $this->clientError(_('There was a problem with your session token.'));
        }
    }

    /**
     * Check if the current request is a POST
     *
     * @return boolean true if POST; otherwise false.
     */

    function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }
    
    function showShareThisButton() {
    	$raw =<<<ENDBUTTON
    	<span  class='st_twitter_large' ></span><span  class='st_facebook_large' ></span><span  class='st_yahoo_large' ></span><span  class='st_gbuzz_large' ></span><span  class='st_email_large' ></span><span  class='st_sharethis_large' ></span>
ENDBUTTON;
		$this->raw($raw);
    }
    
    function showPageNoticeBlock()
    {
    	$rmethod = new ReflectionMethod($this, 'showPageNotice');
    	$dclass = $rmethod->getDeclaringClass()->getName();
    
    	if ($dclass != 'Action' || Event::hasHandler('StartShowPageNotice')) {
    
    		$this->elementStart('dl', array('id' => 'page_notice',
                                                'class' => 'system_notice'));
    		// TRANS: DT element for page notice. String is hidden in default CSS.
    		$this->element('dt', null, _('Page notice'));
    		$this->elementStart('dd');
    		if (Event::handle('StartShowPageNotice', array($this))) {
    			$this->showPageNotice();
    			Event::handle('EndShowPageNotice', array($this));
    		}
    		$this->elementEnd('dd');
    		$this->elementEnd('dl');
    	}
    }
}
