<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class AuthAction extends FluidFrameAction {
	
	function prepare($args) {
		parent::prepare($args);

		// User must be logged in.
		if (!$this->cur) {
			// TRANS: Client error message thrown when trying to access the admin panel while not logged in.
			$this->clientError(_('You are not logged in.'));
			return false;
		}
		return true;
	}

	function showPrimaryNav() {
		$this->elementStart('div',array('id'=>'primarynav'));
		$this->elementStart('ul');
		$this->elementStart('li');
		$this->element('a',array('href'=>common_local_url('logout')),_('Logout'));
		$this->elementEnd('li');
		$this->elementEnd('ul');
		$this->elementEnd('div');
	}
	
}