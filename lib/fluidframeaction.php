<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class FluidFrameAction extends Action {

	var $cur;
	
	function prepare($argarray) {
		parent::prepare($argarray);
		$this->cur = common_current_user();
		return true;
	}
	
	function showLogo() {
		$this->elementStart('div',array('id'=>'logo'));

		$actionUrl = ($this->cur) ? common_local_url('home') : common_local_url('login');
		$this->elementStart('a',array('href'=>$actionUrl));
		$logoUrl = common_config_default('site', 'logo','/images/logo.png');
		$this->element('img', array('class' => 'logo photo',
	    	                                            'src' => $logoUrl,
	    	                                            'alt' => common_config('site', 'name')));
		$this->elementEnd('a');
		$this->elementEnd('div');
	}

	function showFooter() {
		$this->elementStart('div',array('id'=>'footer','class'=>'testo'));
		$this->element('div',null,common_config('site', 'footer'));
		$this->elementEnd('div');
	}
}