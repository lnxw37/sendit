<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

define('FLUIDFRAME_BASE_VERSION', '0.10.');
define('FLUIDFRAME_LIFECYCLE', '1');
define('FLUIDFRAME_VERSION', FLUIDFRAME_BASE_VERSION . FLUIDFRAME_LIFECYCLE);

set_include_path(get_include_path() . PATH_SEPARATOR . INSTALLDIR . '/extlib/');

// Librerie esterne
require_once('PEAR.php');
//require_once('DB/DataObject.php');
//require_once('DB/DataObject/Cast.php'); # for dates
// require_once('markdown.php');

require_once(INSTALLDIR.'/lib/language.php');

require_once(INSTALLDIR.'/lib/event.php');
require_once(INSTALLDIR.'/lib/plugin.php');

function addPlugin($name, $attrs = null)
{
    return Fluidframe::addPlugin($name, $attrs);
}

function _have_config() {
    return Fluidframe::haveConfig();
}

function autoload_sn($cls) {
    if (file_exists(INSTALLDIR.'/classes/' . $cls . '.php')) {
        require_once(INSTALLDIR.'/classes/' . $cls . '.php');
    } else if (file_exists(INSTALLDIR.'/lib/' . strtolower($cls) . '.php')) {
        require_once(INSTALLDIR.'/lib/' . strtolower($cls) . '.php');
    } else if (mb_substr($cls, -6) == 'Action' &&
               file_exists(INSTALLDIR.'/actions/' . strtolower(mb_substr($cls, 0, -6)) . '.php')) {
        require_once(INSTALLDIR.'/actions/' . strtolower(mb_substr($cls, 0, -6)) . '.php');
    } else {
        Event::handle('Autoload', array(&$cls));
    }
}

spl_autoload_register('autoload_sn');


require_once 'Validate.php';

define('NICKNAME_FMT', VALIDATE_NUM.VALIDATE_ALPHA_LOWER);

require_once INSTALLDIR.'/lib/util.php';
require_once INSTALLDIR.'/lib/action.php';
require_once INSTALLDIR.'/lib/mail.php';

require_once INSTALLDIR.'/lib/clientexception.php';
require_once INSTALLDIR.'/lib/serverexception.php';

try {
    Fluidframe::init(@$server, @$path, @$conffile);
} catch (NoConfigException $e) {
    // XXX: Throw a conniption if database not installed
    // XXX: Find a way to use htmlwriter for this instead of handcoded markup
    // TRANS: Error message displayed when no configuration file was found for a StatusNet installation.
    echo '<p>'. _('No configuration file found.') .'</p>';
    // TRANS: Error message displayed when no configuration file was found for a StatusNet installation.
    // TRANS: Is followed by a list of directories (separated by HTML breaks).
    echo '<p>'. _('I looked for configuration files in the following places:') .'<br /> ';
    echo implode($e->configFiles, '<br />');
    exit;
}
