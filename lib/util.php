<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 *
 * @author: Michele Azzolari michele@fluidware.it
 *
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

// Initialize to false; set to null if none found
$_cur = false;


function common_logged_in() {
	return (!is_null(common_current_user()));
}

function common_current_user() {
	global $_cur;

	if ($_cur === false) {

		if (isset($_COOKIE[session_name()]) || isset($_GET[session_name()])
		|| (isset($_SESSION['userid']) && $_SESSION['userid'])) {
			common_ensure_session();
			$id = isset($_SESSION['userid']) ? $_SESSION['userid'] : false;
			if ($id) {
				$user = Profile::staticGet($id);
				if ($user) {
					$_cur = $user;
					return $_cur;
				}
			}
		}

		// that didn't work; try to remember; will init $_cur to null on failure
		$_cur = common_remembered_user();

		if ($_cur) {
			// XXX: Is this necessary?
			$_SESSION['userid'] = $_cur->id;
		}

	}

	return $_cur;
}


function common_cache_key($extra)
{
	return Cache::key($extra);
}

function common_keyize($str)
{
	return Cache::keyize($str);
}

function common_memcache()
{
	//return Cache::instance();
	return null;
}


function common_is_sensitive($action)
{
	static $sensitive = array(
        'login',
        'upload'
	);
	$ssl = null;

	if (Event::handle('SensitiveAction', array($action, &$ssl))) {
		$ssl = in_array($action, $sensitive);
	}

	return $ssl;
}

function common_mime_type_match($type, $avail)
{
	if(array_key_exists($type, $avail)) {
		return $type;
	} else {
		$parts = explode('/', $type);
		if(array_key_exists($parts[0] . '/*', $avail)) {
			return $parts[0] . '/*';
		} elseif(array_key_exists('*/*', $avail)) {
			return '*/*';
		} else {
			return null;
		}
	}
}


function common_negotiate_type($cprefs, $sprefs)
{
	$combine = array();

	foreach(array_keys($sprefs) as $type) {
		$parts = explode('/', $type);
		if($parts[1] != '*') {
			$ckey = common_mime_type_match($type, $cprefs);
			if($ckey) {
				$combine[$type] = $sprefs[$type] * $cprefs[$ckey];
			}
		}
	}

	foreach(array_keys($cprefs) as $type) {
		$parts = explode('/', $type);
		if($parts[1] != '*' && !array_key_exists($type, $sprefs)) {
			$skey = common_mime_type_match($type, $sprefs);
			if($skey) {
				$combine[$type] = $sprefs[$skey] * $cprefs[$type];
			}
		}
	}

	$bestq = 0;
	$besttype = 'text/html';

	foreach(array_keys($combine) as $type) {
		if($combine[$type] > $bestq) {
			$besttype = $type;
			$bestq = $combine[$type];
		}
	}

	if ('text/html' === $besttype) {
		return "text/html; charset=utf-8";
	}
	return $besttype;
}

/**
 * Check for value in $config[$main][$sub]
 *
 * @param unknown_type $main
 * @param unknown_type $sub
 */
function common_config($main, $sub)
{
	global $config;
	return (array_key_exists($main, $config) &&
	array_key_exists($sub, $config[$main])) ? $config[$main][$sub] : false;
}

function common_config_default($main, $sub,$dflt=false) {
	$ret = common_config($main, $sub);
	return ($ret === FALSE ) ? $dflt : $ret ;
}


function common_log_objstring(&$object)
{
	if (is_null($object)) {
		return "null";
	}
	if (!($object instanceof DB_DataObject)) {
		return "(unknown)";
	}
	$arr = $object->toArray();
	$fields = array();
	foreach ($arr as $k => $v) {
		if (is_object($v)) {
			$fields[] = "$k='".get_class($v)."'";
		} else {
			$fields[] = "$k='$v'";
		}
	}
	$objstring = $object->tableName() . '[' . implode(',', $fields) . ']';
	return $objstring;
}

function common_log_line($priority, $msg)
{
	static $syslog_priorities = array('LOG_EMERG', 'LOG_ALERT', 'LOG_CRIT', 'LOG_ERR',
                                      'LOG_WARNING', 'LOG_NOTICE', 'LOG_INFO', 'LOG_DEBUG');
	return date('Y-m-d H:i:s') . ' ' . $syslog_priorities[$priority] . ': ' . $msg . PHP_EOL;
}

function common_request_id()
{
	$pid = getmypid();
	$server = common_config('site', 'server');
	if (php_sapi_name() == 'cli') {
		$script = basename($_SERVER['PHP_SELF']);
		return "$server:$script:$pid";
	} else {
		static $req_id = null;
		if (!isset($req_id)) {
			$req_id = substr(md5(mt_rand()), 0, 8);
		}
		if (isset($_SERVER['REQUEST_URI'])) {
			$url = $_SERVER['REQUEST_URI'];
		}
		$method = $_SERVER['REQUEST_METHOD'];
		return "$server:$pid.$req_id $method $url";
	}
}


function common_log_db_error(&$object, $verb, $filename=null) {
	$objstr = common_log_objstring($object);
	$last_error = &PEAR::getStaticProperty('DB_DataObject','lastError');
	if (is_object($last_error)) {
		$msg = $last_error->message;
	} else {
		$msg = 'Unknown error (' . var_export($last_error, true) . ')';
	}
	common_log(LOG_ERR, $msg . '(' . $verb . ' on ' . $objstr . ')', $filename);
}

function common_log($priority, $msg, $filename=null) {
	if(Event::handle('StartLog', array(&$priority, &$msg, &$filename))){
		$msg = (empty($filename)) ? $msg : basename($filename) . ' - ' . $msg;
		$msg = '[' . common_request_id() . '] ' . $msg;
		$logfile = common_config('site', 'logfile');
		if ($logfile) {
			$log = fopen($logfile, "a");
			if ($log) {
				$output = common_log_line($priority, $msg);
				fwrite($log, $output);
				fclose($log);
			}
		} else {
			common_ensure_syslog();
			syslog($priority, $msg);
		}
		Event::handle('EndLog', array($priority, $msg, $filename));
	}
}

function common_debug($msg, $filename=null)
{
	if ($filename) {
		common_log(LOG_DEBUG, basename($filename).' - '.$msg);
	} else {
		common_log(LOG_DEBUG, $msg);
	}
}

function common_timestamp()
{
	return date('YmdHis');
}

function common_ensure_syslog()
{
	static $initialized = false;
	if (!$initialized) {
		openlog(common_config('syslog', 'appname'), 0,
		common_config('syslog', 'facility'));
		$initialized = true;
	}
}


function common_redirect($url, $code=307)
{
	static $status = array(301 => "Moved Permanently",
	302 => "Found",
	303 => "See Other",
	307 => "Temporary Redirect");

	header('HTTP/1.1 '.$code.' '.$status[$code]);
	header("Location: $url");

	$xo = new XMLOutputter();
	$xo->startXML('a',
                  '-//W3C//DTD XHTML 1.0 Strict//EN',
                  'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd');
	$xo->element('a', array('href' => $url), $url);
	$xo->endXML();
	exit;
}

function common_local_url($action, $args=null, $params=null, $fragment=null, $addSession=true)
{
	$r = Router::get();
	$path = $r->build($action, $args, $params, $fragment);

	$ssl = common_is_sensitive($action);

	if (common_config('site','fancy')) {
		$url = common_path(mb_substr($path, 1), $ssl, $addSession);
	} else {
		if (mb_strpos($path, '/index.php') === 0) {
			$url = common_path(mb_substr($path, 1), $ssl, $addSession);
		} else {
			$url = common_path('index.php'.$path, $ssl, $addSession);
		}
	}
	return $url;
}

function common_path($relative, $ssl=false, $addSession=true)
{
	$pathpart = (common_config('site', 'path')) ? common_config('site', 'path')."/" : '';

	if (($ssl && (common_config('site', 'ssl') === 'sometimes'))
	|| common_config('site', 'ssl') === 'always') {
		$proto = 'https';
		if (is_string(common_config('site', 'sslserver')) &&
		mb_strlen(common_config('site', 'sslserver')) > 0) {
			$serverpart = common_config('site', 'sslserver');
		} else if (common_config('site', 'server')) {
			$serverpart = common_config('site', 'server');
		} else {
			common_log(LOG_ERR, 'Site server not configured, unable to determine site name.');
		}
	} else {
		$proto = 'http';
		if (common_config('site', 'server')) {
			$serverpart = common_config('site', 'server');
		} else {
			common_log(LOG_ERR, 'Site server not configured, unable to determine site name.');
		}
	}

	if ($addSession) {
		$relative = common_inject_session($relative, $serverpart);
	}

	return $proto.'://'.$serverpart.'/'.$pathpart.$relative;
}

function common_have_session()
{
	return (0 != strcmp(session_id(), ''));
}

function common_inject_session($url, $serverpart = null)
{
	if (common_have_session()) {

		if (empty($serverpart)) {
			$serverpart = parse_url($url, PHP_URL_HOST);
		}

		$currentServer = $_SERVER['HTTP_HOST'];

		// Are we pointing to another server (like an SSL server?)

		if (!empty($currentServer) &&
		0 != strcasecmp($currentServer, $serverpart)) {
			// Pass the session ID as a GET parameter
			$sesspart = session_name() . '=' . session_id();
			$i = strpos($url, '?');
			if ($i === false) {
				// no GET params, just append
				$url .= '?' . $sesspart;
			} else {
				$url = substr($url, 0, $i + 1).$sesspart.'&'.substr($url, $i + 1);
			}
		}
	}

	return $url;
}

function common_copy_args($from)
{
	$to = array();
	$strip = get_magic_quotes_gpc();
	foreach ($from as $k => $v) {
		if(is_array($v)) {
			$to[$k] = common_copy_args($v);
		} else {
			if ($strip) {
				$v = stripslashes($v);
			}
			$to[$k] = strval(common_validate_utf8($v));
		}
	}
	return $to;
}

/* Following functions are copied from MediaWiki GlobalFunctions.php
 * and written by Evan Prodromou. */

function common_accept_to_prefs($accept, $def = '*/*')
{
	// No arg means accept anything (per HTTP spec)
	if(!$accept) {
		return array($def => 1);
	}

	$prefs = array();

	$parts = explode(',', $accept);

	foreach($parts as $part) {
		// FIXME: doesn't deal with params like 'text/html; level=1'
		@list($value, $qpart) = explode(';', trim($part));
		$match = array();
		if(!isset($qpart)) {
			$prefs[$value] = 1;
		} elseif(preg_match('/q\s*=\s*(\d*\.\d+)/', $qpart, $match)) {
			$prefs[$value] = $match[1];
		}
	}

	return $prefs;
}

function common_strtotime($str,$format) {
	$dt = DateTime::createFromFormat($format,$str);
	return $dt->getTimestamp();
}

function common_exact_date($dt) {
	static $_utc;
	static $_siteTz;

	if (!$_utc) {
		$_utc = new DateTimeZone('UTC');
		$_siteTz = new DateTimeZone(common_timezone());
	}

	$dateStr = date('d F Y H:i:s', strtotime($dt));
	$d = new DateTime($dateStr, $_utc);
	$d->setTimezone($_siteTz);
	return $d->format(DATE_RFC850);
}

function common_date_w3dtf($dt)
{
	$dateStr = date('d F Y H:i:s', strtotime($dt));
	$d = new DateTime($dateStr, new DateTimeZone('UTC'));
	$d->setTimezone(new DateTimeZone(common_timezone()));
	return $d->format(DATE_W3C);
}

function common_date_rfc2822($dt)
{
	$dateStr = date('d F Y H:i:s', strtotime($dt));
	$d = new DateTime($dateStr, new DateTimeZone('UTC'));
	$d->setTimezone(new DateTimeZone(common_timezone()));
	return $d->format('r');
}

function common_date_iso8601($dt)
{
	$dateStr = date('d F Y H:i:s', strtotime($dt));
	$d = new DateTime($dateStr, new DateTimeZone('UTC'));
	$d->setTimezone(new DateTimeZone(common_timezone()));
	return $d->format('c');
}
/**
 * Very basic stripping of invalid UTF-8 input text.
 *
 * @param string $str
 * @return mixed string or null if invalid input
 *
 * @todo ideally we should drop bad chars, and maybe do some of the checks
 *       from common_xml_safe_str. But we can't strip newlines, etc.
 * @todo Unicode normalization might also be useful, but not needed now.
 */
function common_validate_utf8($str)
{
	// preg_replace will return NULL on invalid UTF-8 input.
	//
	// Note: empty regex //u also caused NULL return on some
	// production machines, but none of our test machines.
	//
	// This should be replaced with a more reliable check.
	return preg_replace('/\x00/u', '', $str);
}

function common_perf_counter($key, $val=null)
{
	global $_perfCounters;
	if (isset($_perfCounters)) {
		if (common_config('site', 'logperf')) {
			if (array_key_exists($key, $_perfCounters)) {
				$_perfCounters[$key][] = $val;
			} else {
				$_perfCounters[$key] = array($val);
			}
			if (common_config('site', 'logperf_detail')) {
				common_log(LOG_DEBUG, "PERF COUNTER HIT: $key $val");
			}
		}
	}
}

function common_log_perf_counters()
{
	if (common_config('site', 'logperf')) {
		global $_startTime, $_perfCounters;

		if (isset($_startTime)) {
			$endTime = microtime(true);
			$diff = round(($endTime - $_startTime) * 1000);
			common_log(LOG_DEBUG, "PERF runtime: ${diff}ms");
	}
	$counters = $_perfCounters;
	ksort($counters);
	foreach ($counters as $key => $values) {
		$count = count($values);
		$unique = count(array_unique($values));
		common_log(LOG_DEBUG, "PERF COUNTER: $key $count ($unique unique)");
	}
}
}

function common_ensure_session()
{
	$c = null;
	if (array_key_exists(session_name(), $_COOKIE)) {
		$c = $_COOKIE[session_name()];
	}
	if (!common_have_session()) {
		if (common_config('sessions', 'handle')) {
			Session::setSaveHandler();
		}
		if (array_key_exists(session_name(), $_GET)) {
			$id = $_GET[session_name()];
		} else if (array_key_exists(session_name(), $_COOKIE)) {
			$id = $_COOKIE[session_name()];
		}
		if (isset($id)) {
			session_id($id);
		}
		@session_start();
		if (!isset($_SESSION['started'])) {
			$_SESSION['started'] = time();
			if (!empty($id)) {
				common_log(LOG_WARNING, 'Session cookie "' . $_COOKIE[session_name()] . '" ' .
                           ' is set but started value is null');
			}
		}
	}
}

function common_session_token()
{
	common_ensure_session();
	if (!array_key_exists('token', $_SESSION)) {
		$_SESSION['token'] = common_good_rand(64);
	}
	return $_SESSION['token'];
}

function common_good_rand($bytes)
{
	// XXX: use random.org...?
	if (@file_exists('/dev/urandom')) {
		return common_urandom($bytes);
	} else { // FIXME: this is probably not good enough
		return common_mtrand($bytes);
	}
}

function common_user_uri(&$user)
{
	return common_local_url('userbyid', array('id' => $user->id),
	null, null, false);
}

function common_urandom($bytes)
{
	$h = fopen('/dev/urandom', 'rb');
	// should not block
	$src = fread($h, $bytes);
	fclose($h);
	$enc = '';
	for ($i = 0; $i < $bytes; $i++) {
		$enc .= sprintf("%02x", (ord($src[$i])));
	}
	return $enc;
}

function common_mtrand($bytes)
{
	$enc = '';
	for ($i = 0; $i < $bytes; $i++) {
		$enc .= sprintf("%02x", mt_rand(0, 255));
	}
	return $enc;
}

function common_real_login($real=true)
{
	common_ensure_session();
	$_SESSION['real_login'] = $real;
}

function common_is_real_login()
{
	return common_logged_in() && $_SESSION['real_login'];
}

function common_check_user($email, $password) {
	// empty $email always unacceptable
	if (empty($email)) {
		return false;
	}

	$authenticatedUser = false;

	if (Event::handle('StartCheckPassword', array($email, $password, &$authenticatedUser))) {
		$user = Profile::staticGet('email', $email);
		if (!empty($user)) {
			if (!empty($password)) {
				// never allow login with blank password
				if (0 == strcmp(common_munge_password($password, $user->id),
				$user->password)) {
					//internal checking passed
					$authenticatedUser = $user;
				}
			}
		}
		Event::handle('EndCheckPassword', array($email, $password, $authenticatedUser));
	}

	return $authenticatedUser;
}

function common_munge_password($password, $id)
{
	if (is_object($id) || is_object($password)) {
		$e = new Exception();
		common_log(LOG_ERR, __METHOD__ . ' object in param to common_munge_password ' .
		str_replace("\n", " ", $e->getTraceAsString()));
	}
	return md5($password . $id);
}

function common_markup_to_html($c)
{
	$c = preg_replace('/%%action.(\w+)%%/e', "common_local_url('\\1')", $c);
	$c = preg_replace('/%%doc.(\w+)%%/e', "common_local_url('doc', array('title'=>'\\1'))", $c);
	$c = preg_replace('/%%(\w+).(\w+)%%/e', 'common_config(\'\\1\', \'\\2\')', $c);
	return Markdown($c);
}

function common_set_user($user)
{
	global $_cur;

	if (is_null($user) && common_have_session()) {
		$_cur = null;
		unset($_SESSION['profile'.$_cur->id]);
		unset($_SESSION['userid']);
		return true;
	} else if (is_string($user)) {
		$email = $user;
		$user = Profile::staticGet('email', $email);
	} else if (!($user instanceof Profile)) {
		return false;
	}

	if ($user) {
		if (Event::handle('StartSetUser', array(&$user))) {
			if (!empty($user)) {
				if (!$user->hasRight(Right::WEBLOGIN)) {
					throw new AuthorizationException(_('Not allowed to log in.'));
				}
				common_ensure_session();
				$_SESSION['userid'] = $user->id;
				$_SESSION['profile'.$user->id] = $user->toJsonString();
				$_cur = $user;
				Event::handle('EndSetUser', array($user));
				return $_cur;
			}
		}
	}
	return false;
}

function common_set_cookie($key, $value, $expiration=0)
{
	$path = common_config('site', 'path');
	$server = common_config('site', 'server');

	if ($path && ($path != '/')) {
		$cookiepath = '/' . $path . '/';
	} else {
		$cookiepath = '/';
	}
	return setcookie($key,
	$value,
	$expiration,
	$cookiepath,
	$server,
	common_config('site', 'ssl')=='always');
}

define('REMEMBERME', 'rememberme');
define('REMEMBERME_EXPIRY', 30 * 24 * 60 * 60); // 30 days

function common_rememberme($user=null)
{
	if (!$user) {
		$user = common_current_user();
		if (!$user) {
			return false;
		}
	}

	$rm = new Remember_me();

	$rm->code = common_good_rand(16);
	$rm->user_id = $user->id;

	// Wrap the insert in some good ol' fashioned transaction code

	$rm->query('BEGIN');

	$result = $rm->insert();

	if (!$result) {
		common_log_db_error($rm, 'INSERT', __FILE__);
		return false;
	}

	$rm->query('COMMIT');

	$cookieval = $rm->user_id . ':' . $rm->code;

	common_log(LOG_INFO, 'adding rememberme cookie "' . $cookieval . '" for ' . $user->nickname);

	common_set_cookie(REMEMBERME, $cookieval, time() + REMEMBERME_EXPIRY);

	return true;
}

function common_remembered_user() {
	$user = null;

	$packed = isset($_COOKIE[REMEMBERME]) ? $_COOKIE[REMEMBERME] : null;

	if (!$packed) {
		return null;
	}

	list($id, $code) = explode(':', $packed);

	if (!$id || !$code) {
		common_log(LOG_WARNING, 'Malformed rememberme cookie: ' . $packed);
		common_forgetme();
		return null;
	}

	$rm = Remember_me::staticGet($code);

	if (!$rm) {
		common_log(LOG_WARNING, 'No such remember code: ' . $code);
		common_forgetme();
		return null;
	}

	if ($rm->user_id != $id) {
		common_log(LOG_WARNING, 'Rememberme code for wrong user: ' . $rm->user_id . ' != ' . $id);
		common_forgetme();
		return null;
	}

	$user = Profile::staticGet($rm->user_id);

	if (!$user) {
		common_log(LOG_WARNING, 'No such user for rememberme: ' . $rm->user_id);
		common_forgetme();
		return null;
	}

	// successful!
	$result = $rm->delete();

	if (!$result) {
		common_log_db_error($rm, 'DELETE', __FILE__);
		common_log(LOG_WARNING, 'Could not delete rememberme: ' . $code);
		common_forgetme();
		return null;
	}

	common_log(LOG_INFO, 'logging in ' . $user->nickname . ' using rememberme code ' . $rm->code);

	common_set_user($user);
	common_real_login(false);

	// We issue a new cookie, so they can log in
	// automatically again after this session

	common_rememberme($user);

	return $user;
}

/**
 * must be called with a valid user!
 */
function common_forgetme()
{
	common_set_cookie(REMEMBERME, '', 0);
}

function common_get_returnto() {
	common_ensure_session();
	return (array_key_exists('returnto', $_SESSION)) ? $_SESSION['returnto'] : null;
}

function common_sql_now()
{
	return common_sql_date(time());
}

function common_sql_date($datetime)
{
	return strftime('%Y-%m-%d %H:%M:%S', $datetime);
}

/**
 * Return an SQL fragment to calculate an age-based weight from a given
 * timestamp or datetime column.
 *
 * @param string $column name of field we're comparing against current time
 * @param integer $dropoff divisor for age in seconds before exponentiation
 * @return string SQL fragment
 */
function common_sql_weight($column, $dropoff)
{
	if (common_config('db', 'type') == 'pgsql') {
		// PostgreSQL doesn't support timestampdiff function.
		// @fixme will this use the right time zone?
		// @fixme does this handle cross-year subtraction correctly?
		return "sum(exp(-extract(epoch from (now() - $column)) / $dropoff))";
	} else {
		return "sum(exp(timestampdiff(second, utc_timestamp(), $column) / $dropoff))";
	}
}

function common_canonical_email($email)
{
	// XXX: canonicalize UTF-8
	// XXX: lcase the domain part
	return $email;
}


function common_database_tablename($tablename)
{
	if(common_config('db','quote_identifiers')) {
		$tablename = '"'. $tablename .'"';
	}
	//table prefixes could be added here later
	return $tablename;
}

function common_confirmation_code($bits)
{
	// 36 alphanums - lookalikes (0, O, 1, I) = 32 chars = 5 bits
	static $codechars = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
	$chars = ceil($bits/5);
	$code = '';
	for ($i = 0; $i < $chars; $i++) {
		// XXX: convert to string and back
		$num = hexdec(common_good_rand(1));
		// XXX: randomness is too precious to throw away almost
		// 40% of the bits we get!
		$code .= $codechars[$num%32];
	}
	return $code;
}

function common_profile_uri($profile)
{
	if (!$profile) {
		return null;
	}
	$user = Profile::staticGet($profile->id);
	if ($user) {
		return $user->uri;
	}

	$remote = Remote_profile::staticGet($profile->id);
	if ($remote) {
		return $remote->uri;
	}
	// XXX: this is a very bad profile!
	return null;
}

function common_profile_url($nickname)
{
	return common_local_url('showstream', array('nickname' => $nickname),
	null, null, false);
}

function common_set_returnto($url)
{
	common_ensure_session();
	$_SESSION['returnto'] = $url;
}

function common_date_sql($sqldate) {
	return strftime('%d/%m/%Y',strtotime($sqldate));
}
function common_datetime_sql($sqldate) {
	return strftime('%d/%m/%Y %H:%M:%S',strtotime($sqldate));
}

/**
 * This should only be used at setup; processes switching languages
 * to send text to other users should use common_switch_locale().
 *
 * @param string $language Locale language code (optional; empty uses
 *                         current user's preference or site default)
 * @return mixed success
 */
function common_init_locale($language=null)
{
	if(!$language) {
		$language = common_language();
	}
	putenv('LANGUAGE='.$language);
	putenv('LANG='.$language);
	$ok =  setlocale(LC_ALL, $language . ".utf8",
	$language . ".UTF8",
	$language . ".utf-8",
	$language . ".UTF-8",
	$language);
	return $ok;
}

/**
 * Initialize locale and charset settings and gettext with our message catalog,
 * using the current user's language preference or the site default.
 *
 * This should generally only be run at framework initialization; code switching
 * languages at runtime should call common_switch_language().
 *
 * @access private
 */
function common_init_language() {
	mb_internal_encoding('UTF-8');

	// Note that this setlocale() call may "fail" but this is harmless;
	// gettext will still select the right language.
	$language = common_language();
	$locale_set = common_init_locale($language);

	if (!$locale_set) {
		// The requested locale doesn't exist on the system.
		//
		// gettext seems very picky... We first need to setlocale()
		// to a locale which _does_ exist on the system, and _then_
		// we can set in another locale that may not be set up
		// (say, ga_ES for Galego/Galician) it seems to take it.
		//
		// For some reason C and POSIX which are guaranteed to work
		// don't do the job. en_US.UTF-8 should be there most of the
		// time, but not guaranteed.
		$ok = common_init_locale("en_US");
		if (!$ok && strtolower(substr(PHP_OS, 0, 3)) != 'win') {
			// Try to find a complete, working locale on Unix/Linux...
			// @fixme shelling out feels awfully inefficient
			// but I don't think there's a more standard way.
			$all = `locale -a`;
			foreach (explode("\n", $all) as $locale) {
				if (preg_match('/\.utf[-_]?8$/i', $locale)) {
					$ok = setlocale(LC_ALL, $locale);
					if ($ok) {
						break;
					}
				}
			}
		}
		if (!$ok) {
			common_log(LOG_ERR, "Unable to find a UTF-8 locale on this system; UI translations may not work.");
		}
		$locale_set = common_init_locale($language);
	}

	common_init_gettext();
}

/**
 * @access private
 */
function common_init_gettext()
{
	setlocale(LC_CTYPE, 'C');
	// So we do not have to make people install the gettext locales
	$path = common_config('site','locale_path');
	bindtextdomain("sendit", $path);
	bind_textdomain_codeset("sendit", "UTF-8");
	textdomain("sendit");
}

/**
 * Switch locale during runtime, and poke gettext until it cries uncle.
 * Otherwise, sometimes it doesn't actually switch away from the old language.
 *
 * @param string $language code for locale ('en', 'fr', 'pt_BR' etc)
 */
function common_switch_locale($language=null)
{
	common_init_locale($language);

	setlocale(LC_CTYPE, 'C');
	// So we do not have to make people install the gettext locales
	$path = common_config('site','locale_path');
	bindtextdomain("sendit", $path);
	bind_textdomain_codeset("sendit", "UTF-8");
	textdomain("sendit");
}

function common_timezone()
{
	if (common_logged_in()) {
		$user = common_current_user();
		if ($user->timezone) {
			return $user->timezone;
		}
	}

	return common_config('site', 'timezone');
}

function common_language()
{
	// If there is a user logged in and they've set a language preference
	// then return that one...
	if (_have_config() && common_logged_in()) {
		$user = common_current_user();
		$user_language = $user->language;

		if ($user->language) {
			// Validate -- we don't want to end up with a bogus code
			// left over from some old junk.
			foreach (common_config('site', 'languages') as $code => $info) {
				if ($info['lang'] == $user_language) {
					return $user_language;
				}
			}
		}
	}

	// Otherwise, find the best match for the languages requested by the
	// user's browser...
	if (common_config('site', 'langdetect')) {
		$httplang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : null;
		if (!empty($httplang)) {
			$language = client_prefered_language($httplang);
			if ($language)
			return $language;
		}
	}
	// Finally, if none of the above worked, use the site's default...
	return common_config('site', 'language');
}


function common_xml_safe_str($str)
{
	// Replace common eol and extra whitespace input chars
	$unWelcome = array(
        "\t",  // tab
        "\n",  // newline
        "\r",  // cr
        "\0",  // null byte eos
        "\x0B" // vertical tab
	);

	$replacement = array(
        ' ', // single space
        ' ',
        '',  // nothing
        '',
        ' '
	);

	$str = str_replace($unWelcome, $replacement, $str);

	// Neutralize any additional control codes and UTF-16 surrogates
	// (Twitter uses '*')
	return preg_replace('/[\p{Cc}\p{Cs}]/u', '*', $str);
}

function common_bytes_to_size($bytes, $precision = 2) {  

    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
   
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . ' B';
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return $bytes . ' B';
    }

}

function common_error_handler($errno, $errstr, $errfile, $errline, $errcontext)
{
	switch ($errno) {

		case E_ERROR:
		case E_COMPILE_ERROR:
		case E_CORE_ERROR:
		case E_USER_ERROR:
		case E_PARSE:
		case E_RECOVERABLE_ERROR:
			common_log(LOG_ERR, "[$errno] $errstr ($errfile:$errline) [ABORT]");
			die();
			break;

		case E_WARNING:
		case E_COMPILE_WARNING:
		case E_CORE_WARNING:
		case E_USER_WARNING:
			common_log(LOG_WARNING, "[$errno] $errstr ($errfile:$errline)");
			break;

		case E_NOTICE:
		case E_USER_NOTICE:
			common_log(LOG_NOTICE, "[$errno] $errstr ($errfile:$errline)");
			break;

		case E_STRICT:
		case E_DEPRECATED:
		case E_USER_DEPRECATED:
			// XXX: config variable to log this stuff, too
			break;

		default:
			common_log(LOG_ERR, "[$errno] $errstr ($errfile:$errline) [UNKNOWN LEVEL, die()'ing]");
		die();
		break;
	}

	// FIXME: show error page if we're on the Web
	/* Don't execute PHP internal error handler */
	return true;
}
