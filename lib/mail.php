<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

require_once 'Mail.php';

/**
 * return the configured mail backend
 *
 * Uses the $config array to make a mail backend. Cached so it is safe to call
 * more than once.
 *
 * @return Mail backend
 */
function mail_backend()
{
    static $backend = null;

    if (!$backend) {
        $backend = Mail::factory(common_config('mail', 'backend'),
                                 (common_config('mail', 'params')) ?
                                 common_config('mail', 'params') :
                                 array());
        if (PEAR::isError($backend)) {
            common_server_error($backend->getMessage(), 500);
        }
    }
    return $backend;
}

/**
 * send an email to one or more recipients
 *
 * @param array  $recipients array of strings with email addresses of recipients
 * @param array  $headers    array mapping strings to strings for email headers
 * @param string $body       body of the email
 *
 * @return boolean success flag
 */
function mail_send($recipients, $headers, $body)
{
    // XXX: use Mail_Queue... maybe
    $backend = mail_backend();
    if (!isset($headers['Content-Type'])) {
        $headers['Content-Type'] = 'text/plain; charset=UTF-8';
    }
    assert($backend); // throws an error if it's bad
    $sent = $backend->send($recipients, $headers, $body);
    if (PEAR::isError($sent)) {
        common_log(LOG_ERR, 'Email error: ' . $sent->getMessage());
        return false;
    }
    return true;
}

/**
 * returns the configured mail domain
 *
 * Defaults to the server name.
 *
 * @return string mail domain, suitable for making email addresses.
 */
function mail_domain()
{
    $maildomain = common_config('mail', 'domain');
    if (!$maildomain) {
        $maildomain = common_config('site', 'server');
    }
    return $maildomain;
}

/**
 * returns a good address for sending email from this server
 *
 * Uses either the configured value or a faked-up value made
 * from the mail domain.
 *
 * @return string notify from address
 */
function mail_notify_from()
{
    $notifyfrom = common_config('mail', 'notifyfrom');

    if (!$notifyfrom) {

        $domain = mail_domain();

        $notifyfrom = '"'. str_replace('"', '\\"', common_config('site', 'name')) .'" <noreply@'.$domain.'>';
    }

    return $notifyfrom;
}

function mail_notify_to() {
    $notifyto= common_config('site', 'email');
    return $notifyto;
}
/**
 * sends email to a user
 *
 * @param User   &$user   user to send email to
 * @param string $subject subject of the email
 * @param string $body    body of the email
 * @param array  $headers optional list of email headers
 * @param string $address optional specification of email address
 *
 * @return boolean success flag
 */
function mail_to_user(&$profile, $subject, $body, $headers=array(), $address=null)
{
    if (!$address) {
        $address = $profile->email;
    }

    $recipients = $address;
    
    $headers['From']    = mail_notify_from();
    $headers['To']      = $profile->fullname . ' <' . $address . '>';
    $headers['Subject'] = $subject;

    return mail_send($recipients, $headers, $body);
}

function mail_send_notifica_file($from, $name, $destination, $hash, $filename) {
	$headers['From']    = $name . ' <'.$from.'>';
	$headers['To']      = $destination;
	$headers['Subject'] = sprintf("[%1\$s] %2\$s",common_config('site', 'name'),_('New file'));
	$size = filesize(common_config('site', 'upd-path') . '/' . $hash);
	$body = sprintf(_("Hi,\n" .
		"I just shared with you the file %1\$s (%2\$s):\n\n"),$filename, common_bytes_to_size($size));
	$body .= common_local_url('download',array('hash'=>$hash,'filename'=>$filename)) . "\n\n";
	$maxtime = common_config('site','maxtime');
	if($maxtime) {
		$maxhour = round($maxtime / 3600);
	}
	if(common_config('site','deletefile')) {
		$body .= sprintf(_("it will be available for 1 download for %s hours from now\n\n"),$maxhour);
	} else {
		$body .= sprintf(_("it will be available for %s hours from now\n\n"),$maxhour);
	}
	$body .= _("Thanks");
	$body .= sprintf(_("\n%1\$s\n\n---\n%2\$s\n"),$name,common_config('site', 'description'));

	mail_send($destination, $headers, $body);
}

function _mail_prepare_headers($msg_type, $to, $from) {
    $headers = array(
        'X-Fluidframe-Domain'      => common_config('site', 'server')
    );
    return $headers;
}
