<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

$default =
  array('site' =>
        array('name' => 'A Fluidframe instance',
              'nickname' => 'fluidframe',
              'footer' => 'Send It! An open source <a href="http://macno.org/projects/sendit/">project</a> by <a href="http://stream.macno.org/macno">macno</a> ',
              'wildcard' => null,
              'server' => $_server,
              'theme' => 'default',
              'path' => $_path,
              'logfile' => null,
              'logo' => null,
              'ssllogo' => null,
              'logdebug' => false,
              'logperf' => false, // Enable to dump performance counters to syslog
              'logperf_detail' => false, // Enable to dump every counter hit
              'fancy' => false,
              'locale_path' => INSTALLDIR.'/locale',
              'language' => 'en',
              'langdetect' => true,
              'languages' => get_all_languages(),
              'email' =>
              array_key_exists('SERVER_ADMIN', $_SERVER) ? $_SERVER['SERVER_ADMIN'] : null,
              'broughtby' => null,
              'timezone' => 'UTC',
              'broughtbyurl' => null,
              'closed' => false,
              'inviteonly' => false,
              'private' => false,
              'ssl' => 'never',
              'sslserver' => null,
              'shorturllength' => 30,
              'dupelimit' => 60, // default for same person saying the same thing
              'textlimit' => 140,
              'indent' => true,
              'use_x_sendfile' => false,
              'notice' => null, // site wide notice text
              'build' => 1, // build number, for code-dependent cache,
              'deletefile' => true,
              'upd-path' => 'YOU HAVE TO SET THIS IN config.php',
              ),
        'syslog' =>
        array('appname' => 'fluidframe', # for syslog
              'priority' => 'debug', # XXX: currently ignored
              'facility' => LOG_USER),

        'license' =>
        array('type' => 'private', # can be 'cc', 'allrightsreserved', 'private'
              'owner' => null, # can be name of content owner e.g. for enterprise
              'url' => 'http://creativecommons.org/licenses/by/3.0/',
              'title' => 'Creative Commons Attribution 3.0',
              'image' => 'http://i.creativecommons.org/l/by/3.0/80x15.png'),
        'mail' =>
        array('backend' => 'mail',
              'params' => null,
              'domain_check' => true),
        'public' =>
        array('localonly' => true,
              'blacklist' => array(),
              'autosource' => array()),
        'theme' =>
        array('server' => null,
              'dir' => null,
              'path'=> null,
              'ssl' => null),
        'theme_upload' =>
        array('enabled' => extension_loaded('zip')),
        'javascript' =>
        array('server' => null,
              'path'=> null,
              'ssl' => null,
              'bustframes' => true),
        'local' => // To override path/server for themes in 'local' dir (not currently applied to local plugins)
        array('server' => null,
              'dir' => null,
              'path' => null,
              'ssl' => null),
        'throttle' =>
        array('enabled' => false, // whether to throttle edits; false by default
              'count' => 20, // number of allowed messages in timespan
              'timespan' => 600), // timespan for throttling
        'daemon' =>
        array('piddir' => '/var/run',
              'user' => false,
              'group' => false),
        'emailpost' =>
        array('enabled' => true),
        'cache' =>
        array('base' => null),
        'ping' =>
        array('notify' => array(),
              'timeout' => 2),
        'application' =>
        array('desclimit' => null),
        'group' =>
        array('maxaliases' => 3,
              'desclimit' => null),
        'oohembed' => array('endpoint' => 'http://oohembed.com/oohembed/'),
        'search' =>
        array('type' => 'fulltext'),
        'sessions' =>
        array('handle' => false,   // whether to handle sessions ourselves
              'debug' => false,    // debugging output for sessions
              'gc_limit' => 1000), // max sessions to expire at a time
        'plugins' =>
        array('default' => array(),
              'locale_path' => false, // Set to a path to use *instead of* each plugin's own locale subdirectories
              'server' => null,
              'sslserver' => null,
              'path' => null,
              'sslpath' => null,
              ),
        'robotstxt' =>
        array('crawldelay' => 0,
              'disallow' => array('main', 'settings', 'admin', 'search', 'message')
              ),
        'api' =>
        array('realm' => null),
        'nofollow' =>
        array('subscribers' => true,
              'members' => true,
              'peopletag' => true,
              'external' => 'sometimes'), // Options: 'sometimes', 'never', default = 'sometimes'
        'http' => // HTTP client settings when contacting other sites
        array('ssl_cafile' => false, // To enable SSL cert validation, point to a CA bundle (eg '/usr/lib/ssl/certs/ca-certificates.crt')
              'curl' => false, // Use CURL backend for HTTP fetches if available. (If not, PHP's socket streams will be used.)
              'proxy_host' => null,
              'proxy_port' => null,
              'proxy_user' => null,
              'proxy_password' => null,
              'proxy_auth_scheme' => null,
              ),
	'router' =>
	array('cache' => true), // whether to cache the router object. Defaults to true, turn off for devel
        );
