<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

require_once 'Net/URL/Mapper.php';

class Hologgraphy_URL_Mapper extends Net_URL_Mapper
{
    private static $_singleton = null;
    private $_actionToPath = array();

    private function __construct() {
    }
    
    public static function getInstance($id = '__default__') {
        if (empty(self::$_singleton)) {
            self::$_singleton = new Hologgraphy_URL_Mapper();
        }
        return self::$_singleton;
    }

    public function connect($path, $defaults = array(), $rules = array())
    {
        $result = null;
        if (Event::handle('StartConnectPath', array(&$path, &$defaults, &$rules, &$result))) {
            $result = parent::connect($path, $defaults, $rules);
            if (array_key_exists('action', $defaults)) {
                $action = $defaults['action'];
            } elseif (array_key_exists('action', $rules)) {
                $action = $rules['action'];
            } else {
                $action = null;
            }
            $this->_mapAction($action, $result);
            Event::handle('EndConnectPath', array($path, $defaults, $rules, $result));
        }
        return $result;
    }
    
    protected function _mapAction($action, $path)
    {
        if (!array_key_exists($action, $this->_actionToPath)) {
            $this->_actionToPath[$action] = array();
        }
        $this->_actionToPath[$action][] = $path;
        return;
    }
    
    public function generate($values = array(), $qstring = array(), $anchor = '')
    {
        if (!array_key_exists('action', $values)) {
            return parent::generate($values, $qstring, $anchor);
        }
	
        $action = $values['action'];

        if (!array_key_exists($action, $this->_actionToPath)) {
            return parent::generate($values, $qstring, $anchor);
        }
	
        $oldPaths    = $this->paths;
        $this->paths = $this->_actionToPath[$action];
        $result      = parent::generate($values, $qstring, $anchor);
        $this->paths = $oldPaths;

        return $result;
    }
}

/**
 * URL Router
 *
 * Cheap wrapper around Net_URL_Mapper
 *
 */
class Router
{
    var $m = null;
    static $inst = null;

    const REGEX_TAG = '[^\/]+'; // [\pL\pN_\-\.]{1,64} better if we can do unicode regexes

    static function get()
    {
        if (!Router::$inst) {
            Router::$inst = new Router();
        }
        return Router::$inst;
    }

    function __construct()
    {
        if (empty($this->m)) {
            if (!common_config('router', 'cache')) {
                $this->m = $this->initialize();
            } else {
                $k = self::cacheKey();
                $c = Cache::instance();
                $m = $c->get($k);
                if (!empty($m)) {
                    $this->m = $m;
                } else {
                    $this->m = $this->initialize();
                    $c->set($k, $this->m);
                }
            }
        }
    }

    /**
     * Create a unique hashkey for the router.
     * 
     * The router's url map can change based on the version of the software
     * you're running and the plugins that are enabled. To avoid having bad routes
     * get stuck in the cache, the key includes a list of plugins and the software
     * version.
     * 
     * There can still be problems with a) differences in versions of the plugins and 
     * b) people running code between official versions, but these tend to be more
     * sophisticated users who can grok what's going on and clear their caches.
     * 
     * @return string cache key string that should uniquely identify a router
     */
    
    static function cacheKey()
    {
        return null;
    }
    
    function initialize()
    {
        $m = Hologgraphy_URL_Mapper::getInstance();

        if (Event::handle('StartInitializeRouter', array(&$m))) {
            $m->connect('robots.txt', array('action' => 'robotstxt'));
            $m->connect('', array('action' => 'public'));
            $m->connect('home',array('action'=>'home'));
			$m->connect('login',array('action'=>'login'));
			$m->connect('logout',array('action'=>'logout'));
			$m->connect('upload',array('action'=>'upload'));
			$m->connect('progress',array('action'=>'progress'));
			$m->connect(':hash/:filename',array('action'=>'download'));
            Event::handle('RouterInitialized', array($m));
        }

        return $m;
    }

    function map($path)
    {
        try {
            $match = $this->m->match($path);
        } catch (Net_URL_Mapper_InvalidException $e) {
            common_log(LOG_ERR, "Problem getting route for $path - " .
                       $e->getMessage());
            // TRANS: Client error on action trying to visit a non-existing page.
            $cac = new ClientErrorAction(_('Page not found.'), 404);
            $cac->showPage();
        }

        return $match;
    }

    function build($action, $args=null, $params=null, $fragment=null)
    {
        $action_arg = array('action' => $action);

        if ($args) {
            $args = array_merge($action_arg, $args);
        } else {
            $args = $action_arg;
        }

        $url = $this->m->generate($args, $params, $fragment);

        // Due to a bug in the Net_URL_Mapper code, the returned URL may
        // contain a malformed query of the form ?p1=v1?p2=v2?p3=v3. We
        // repair that here rather than modifying the upstream code...

        $qpos = strpos($url, '?');
        if ($qpos !== false) {
            $url = substr($url, 0, $qpos+1) .
                str_replace('?', '&', substr($url, $qpos+1));

            // @fixme this is a hacky workaround for http_build_query in the
            // lower-level code and bad configs that set the default separator
            // to &amp; instead of &. Encoded &s in parameters will not be
            // affected.
            $url = substr($url, 0, $qpos+1) .
                str_replace('&amp;', '&', substr($url, $qpos+1));

        }

        return $url;
    }
}
