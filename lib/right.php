<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

class Right {
    const CONFIGURESITE      = 'configuresite';
    const DELETEUSER         = 'deleteuser';
    const GRANTROLE          = 'grantrole';
    const REVOKEROLE         = 'revokerole';
    const WEBLOGIN           = 'weblogin';
}

