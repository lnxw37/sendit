var FF = {
		Util: {
			Form: {
				apcEnable: false,
				apcUkey: {},
				UploadProgress: function() {
					setTimeout(function() {
						$.getJSON("/progress?X-Progress-ID="+FF.Util.Form.apcUkey+"&randval="+ Math.random(), {}, 
							function(data) {
							if(data.state == 'uploading') {
								var perc   = Math.round((data.received / data.size) * 100);
								//	var perc = Math.round(parseInt(data.current)/parseInt(data.total)*100);
								$('#progress_container').fadeIn(100);     
								$('#progress_bar').width(perc +"%"); 
								$('#progress_completed').html(perc +"%");
//								if(data.done == 1) {
//									return;
//								}
								if (perc >= 100) {
									return ;
								} else {
									FF.Util.Form.UploadProgress();
								}
							} else if(data.state == 'starting'){
								FF.Util.Form.UploadProgress();
							} else if (data.state == 'done') {
								return;
							}
						});
					},1000);
				},
				UploadIframe: function() {
					$('#form_file_upload_add').iframePostForm
					({
						json : true,
						post : function () {
							$('#form_file_upload_add').hide();
							if(FF.Util.Form.apcEnable) {
								$('#form_file_upload_loading').html('<div id="progress_container"><div id="progress_bar"><div id="progress_completed"></div></div></div>');
							}
							$('#form_file_upload_loading').show();
							FF.Util.Form.UploadProgress();

						},
						complete : function (data) {
							if(data.status == 'ok') {
								$('#form_file_upload_add').html('<p>File Inviato Correttamente!</p><span>'+data.message+'</span>');
								$('#form_file_upload_add').show();
								$('#form_file_upload_loading').hide().html("");
							} else {
								$('#form_file_upload_loading').hide();
								$('#form_file_upload_warning').html('Si &egrave; verificato un errore: <b>' + data.message + '</b>');
								$('#form_file_upload_add').show().html("");
							}
						}
					});
				},
				UploadFormData: function() {

					var data = new FormData();

					jQuery.each($('#filename')[0].files, function(i, file) {
						data.append('filename', file);
					});
					data.append('email',$('#email').val());
					$('#form_file_upload_add').hide();
					$('#form_file_upload_loading').show();
					$.ajax({
						url: '/upload',
						data: data,
						cache: false,
						contentType: false,
						processData: false,
						type: 'POST',
						success: function(data){
							if(data.status == 'ok') {
								$('#form_file_upload_add').html('<p>File Inviato Correttamente!</p><span>'+data.message+'</span>');
								$('#form_file_upload_add').show();
								$('#form_file_upload_loading').hide();
							} else {
								$('#form_file_upload_loading').hide();
								$('#form_file_upload_warning').html('Si &egrave; verificato un errore: <b>' + data.message + '</b>');
								$('#form_file_upload_add').show();
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$('#form_file_upload_loading').hide();
							$('#form_file_upload_warning').html('Si &egrave; verificato un errore: <b>' + errorThrown + '</b>');
							$('#form_file_upload_add').show();
						}
					});
				},
				Init: function() {
					FF.Util.Form.UploadIframe();
//					$('#form_file_upload_add').submit(
//							function() {
//								FF.Util.Form.UploadFormData();
//								return false;
//							});
				}
			}
		}	
};
