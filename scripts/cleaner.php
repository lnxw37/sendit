<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

define('INSTALLDIR', realpath(dirname(__FILE__) . '/..'));

$shortoptions = 't:n:';
$longoptions = array('time=','dry-run');

$helptext = <<<END_OF_EXPORTACTIVITYSTREAM_HELP
cleaner.php [options]
Clean the upload directory according to configuration

  -n --dry-run	perform a trial run with no changes made
  -t --max-time	overwrite the config['site']['maxtime']

END_OF_EXPORTACTIVITYSTREAM_HELP;

require_once INSTALLDIR.'/scripts/commandline.inc';

$dryrun = false;

if (have_option('n', 'dry-run')) {
	$dryrun = true;
}
$maxtime = common_config('site', 'maxtime');
if(have_option('t', 'max-time')) {
	$maxtime = get_option_value('t', 'max-time');
}

$now = time();
$upddir = common_config('site', 'upd-path');
printfnq("Scanning " . $upddir . " for files older than " . $maxtime . " seconds\n");
if ($handle = opendir($upddir)) {
	while (false !== ($entry = readdir($handle))) {
		if ($entry != "." && $entry != "..") {
			if(substr($entry,-5) == '.info') {
				continue;
			} else {
				$mfile = filemtime($upddir.'/'.$entry);
				printfv("Checking " .$entry . "...\n" );
				if( ($now-$mfile) > $maxtime ) {
					printfnq("\tDeleted\n");
					if(!$dryrun) {
						unlink ($upddir.'/'.$entry);
						unlink ($upddir.'/'.$entry.'.info');
					}
				} else {
					printfv("\tNot Deleted\n");
				}
			}
		}
	}
	closedir($handle);
}
if($dryrun)
	printfnq("Executed in dry-run mode\n");