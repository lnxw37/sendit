<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

# Abort if called from a web server

if (isset($_SERVER) && array_key_exists('REQUEST_METHOD', $_SERVER)) {
    print "This script must be run from the command line\n";
    exit();
}

define('FLUIDFRAME', true);

// Set various flags so we don't time out on long-running processes

ini_set("max_execution_time", "0");
ini_set("max_input_time", "0");
set_time_limit(0);
mb_internal_encoding('UTF-8');

// Add extlib to our path so we can get Console_Getopt

$_extra_path = array(INSTALLDIR.'/extlib/');

set_include_path(implode(PATH_SEPARATOR, $_extra_path) . PATH_SEPARATOR . get_include_path());

require_once 'Console/Getopt.php';

// Note: $shortoptions and $longoptions should be pre-defined!

$_default_shortoptions = 'qvhc:s:p:';

$_default_longoptions = array('quiet', 'verbose', 'help', 'conf=', 'server=', 'path=');

if (isset($shortoptions)) {
    $shortoptions .= $_default_shortoptions;
} else {
    $shortoptions = $_default_shortoptions;
}

if (isset($longoptions)) {
    $longoptions = array_merge($longoptions, $_default_longoptions);
} else {
    $longoptions = $_default_longoptions;
}

$parser = new Console_Getopt();

$result = $parser->getopt($argv, $shortoptions, $longoptions);

if (PEAR::isError($result)) {
    print $result->getMessage()."\n";
    exit(1);
} else {
    list($options, $args) = $result;
}

function show_help()
{
    global $helptext;

    $_default_help_text = <<<END_OF_DEFAULT
      General options:

    -q --quiet           Quiet (little output)
    -v --verbose         Verbose (lots of output)
    -c --conf=<filename> Use <filename> as config file
    -s --server=<name>   Use <name> as server name
    -p --path=<path>     Use <path> as path name
    -h --help            Show this message and quit.

END_OF_DEFAULT;
    if (isset($helptext)) {
        print $helptext;
    }
    print $_default_help_text;
    exit(0);
}

foreach ($options as $option) {

    switch ($option[0]) {
     case '--server':
     case 's':
        $server = $option[1];
        break;

     case '--path':
     case 'p':
        $path = $option[1];
        break;

     case '--conf':
     case 'c':
        $conffile = $option[1];
        break;

     case '--help':
     case 'h':
        show_help();
    }
}

require_once INSTALLDIR . '/lib/common.php';

set_error_handler('common_error_handler');

// Set up the language infrastructure so we can localize anything that
// needs to be sent out to users, such as mail notifications.
common_init_language();

function _make_matches($opt, $alt)
{
    $matches = array();

    if (strlen($opt) > 1 && 0 != strncmp($opt, '--', 2)) {
        $matches[] = '--'.$opt;
    } else {
        $matches[] = $opt;
    }

    if (!empty($alt)) {
        if (strlen($alt) > 1 && 0 != strncmp($alt, '--', 2)) {
            $matches[] = '--'.$alt;
        } else {
            $matches[] = $alt;
        }
    }

    return $matches;
}

function have_option($opt, $alt=null)
{
    global $options;

    $matches = _make_matches($opt, $alt);

    foreach ($options as $option) {
        if (in_array($option[0], $matches)) {
            return true;
        }
    }

    return false;
}

function get_option_value($opt, $alt=null)
{
    global $options;

    $matches = _make_matches($opt, $alt);

    foreach ($options as $option) {
        if (in_array($option[0], $matches)) {
            return $option[1];
        }
    }

    return null;
}

/** "Printf not quiet" */

function printfnq()
{
    if (have_option('q', 'quiet')) {
        return null;
    }

    $cargs  = func_num_args();

    if ($cargs == 0) {
        return 0;
    }

    $args   = func_get_args();
    $format = array_shift($args);

    return vprintf($format, $args);
}

/** "Print when verbose" */

function printfv()
{
    if (!have_option('v', 'verbose')) {
        return null;
    }

    $cargs  = func_num_args();

    if ($cargs == 0) {
        return 0;
    }

    $args   = func_get_args();
    $format = array_shift($args);

    return vprintf($format, $args);
}