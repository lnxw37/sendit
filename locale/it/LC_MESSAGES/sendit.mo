��    %      D  5   l      @     A     R     b     r  4        �     �  2   �  9        ?     _     e     l     {     �     �  H   �     �          "     2     B     R     o  2   v  G   �  ,   �  ?        ^     m     z     �     �     �  ,   �  ;   �  1       K     \     p     �  F   �     �     �  0   �  &   	     D	     a	     h	     m	     �	  
   �	  #   �	  S   �	     (
     B
     S
     f
     �
  E   �
     �
  5   �
  V     :   v  N   �                &     :  	   A     K  *   `  9   �                                                 	   %   $            !   "                                                                    #                       
                  
%1$s

---
%2$s
 ..please wait.. Already Logged. BUTTONLogin Error setting user. You are probably not authorized. File does not exist. Hello %s Hi,
I just shared with you the file %1$s (%2$s):

 I looked for configuration files in the following places: Incorrect username or password. Login Logout Lost our file. Max size %s bytes New file No configuration file found. No configuration file found. Try running the installation program first. Not allowed to log in. Not logged in. Page not found. Partial upload. Recipient Email System error uploading file. Thanks That file is too big. The maximum file size is %s. The uploaded file exceeds the upload_max_filesize directive in php.ini. There was a problem with your session token. There was a problem with your session token. Try again, please. Unknown action Unknown page Untitled page Username Welcome You are not logged in. it will be available for %s hours from now

 it will be available for 1 download for %s hours from now

 Project-Id-Version: Sendit
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-03-14 16:18+0100
PO-Revision-Date: 2012-03-26 16:14+0100
Last-Translator: Michele Azzolari <michele@fluidware.it>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
%1$s

---
%2$s
 ..attendere prego.. Utente già loggato Accedi Errore nel salvataggio dell'utente. Probabilmente non sei autorizzato. File inesistente Salve %s Salve,
ho appena caricato il file %1$s (%2$s):

 Ho cercato il file nei seguenti posti: Utente o password non validi Accedi Esci Non trovo il file caricato. Dimensione massima %s bytes Nuovo file File di configurazione non trovato. File di configurazione non trovato. Prova ad eseguire il programma di installazione Non abilitato all'accesso Non autenticato. Pagina non trovata Caricamento del file parziale. Email del destinatario Si è verificato un errore di sistema durante il caricamento del file Grazie Il file è troppo grande. La dimensione massima è %s Il file caricato ha una dimensione superiore al valore upload_max_filesize nel php.ini Si è verificato un problema con il codice della sessione. Si è verificato un problema con il codice della sessione. Riprova per favore. Azione sconosciuta Pagina sconosciuta Pagina senza titolo Utente Benvenuto Non sei autenticato. sarà disponibile per le prossime %s ore

 sarà disponibile per 1 download per le prossime %s ore

 