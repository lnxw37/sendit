<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) { exit(1); }

/**
 * Prints out a static robots.txt
 * 
 */
class RobotstxtAction extends Action
{
    /**
     * Handles requests
     *
     * Since this is a relatively static document, we
     * don't do a prepare()
     *
     * @param array $args GET, POST, and URL params; unused.
     *
     * @return void
     */
    function handle($args)
    {
        if (Event::handle('StartRobotsTxt', array($this))) {
            header('Content-Type: text/plain');
            print "User-Agent: *\n";
            print "Disallow: /\n";
            Event::handle('EndRobotsTxt', array($this));
        }
    }

    /**
     * Return true; this page doesn't touch the DB.
     *
     * @param array $args other arguments
     *
     * @return boolean is read only action?
     */
    function isReadOnly($args)
    {
        return true;
    }
}
