<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class PublicAction extends FluidFrameAction {

	function title() {
		return _("Welcome");
	}
	
	function prepare($args) {
		parent::prepare($args);
		if($this->cur) {
			common_redirect(common_local_url('home'),303);
		} else {
			common_redirect(common_local_url('login'),303);
		}
		return true;
	}
}