<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class DownloadAction extends FluidFrameAction {
	
	function handle($args) {
		$hash = $this->arg('hash');
		$filename = $this->arg('filename');
		
		$file = common_config('site', 'upd-path') . '/' . $hash;
		if(!file_exists($file)) {
			$this->clientError(_('File does not exist.'));
			return;
		}
		$checkFile = file_get_contents($file.'.info');
		
		if(trim($checkFile) != urldecode($filename)) {
			$this->clientError(_('File does not exist.'));
			return;
		}
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$checkFile);
		
		$handle = fopen($file, "rb");
		$outstream = fopen("php://output", 'w');
		$contents = null;
		while (!feof($handle)) {
			$contents = fread($handle, 8192);
			fwrite($outstream, $contents);
		}
		fclose($outstream);
		fclose($handle);
		if(common_config_default('site', 'deletefile',true)) {
			unlink($file);
			unlink($file.'.info');
		}
	}
}