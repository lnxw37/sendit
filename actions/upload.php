<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class UploadAction extends AuthAction {

	function prepare($argarray) {
		parent::prepare($argarray);
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			common_redirect(common_local_url('home'),303);
			return false;
		}
		return true;
	}

	function handle($args) {
		parent::handle($args);
		$this->trySave();
	}

	function trySave() {
		//header('Content-Type: application/json; charset=utf-8');
		$email = $this->trimmed('email');
		if(empty($email)) {
			$this->showError('Il campo email deve essere compilato.');
		} else if(!Validate::email($email)) {
			$this->showError('Il campo email non &egrave; valido.');
		} else {
			$hash = common_good_rand(32);
			try {
				$ok = false;
				$filename = '';
				if (Event::handle('StartCheckFileUpload', array(&$this,&$ok,&$filename, $hash))) {
					$ok = MediaFile::fromUpload($hash,'filename');
					Event::handle('EndCheckFileUpload',array(&$this,&$ok,$hash));
				}
				if($ok) {
					$ret = array('status'=>'ok','message'=>'ID: ' . $hash);
					mail_send_notifica_file($this->cur->email, $this->cur->fullname, $email, $hash, $filename);
					echo json_encode($ret);
				}
				
			} catch (Exception $e) {
				$this->showError($e->getMessage());
			}
		}
	}
	
	function showError($message) {
		$ret = array('status'=>'ko','message'=>$message);
		echo json_encode($ret);
	}
	
}