<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class ProgressAction extends AuthAction {
	
	function handle($args) {
		parent::handle($args);
		$ukey = $this->trimmed('X-Progress-ID');
		$status = apc_fetch('upload_'.$ukey);
// 		common_debug('Status: ' . print_r($status,true));
		// Formatting response to be compatible with nginx upload progress module
		// data.state
		// data.received
		// data.size
		$state = 'uploading';
		if($status['current'] == 0) {
			$state = 'starting';
		} else if ($status['current'] == $status['total']) {
			$state = 'done';
		}
		$ret = array(
			'state' => $state,
			'received' => $status['current'],
			'size' => $status['total']
		);
		echo json_encode($ret);
	}

}