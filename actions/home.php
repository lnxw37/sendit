<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class HomeAction extends AuthAction {

	var $apcEnable;
	var $apcUkey;
	
	function title() {
		return _("Upload");
	}
	
	function prepare($args) {
		parent::prepare($args);
		$this->apcEnable = (extension_loaded('apc') && ini_get("apc.rfc1867"));
		return true;
	}
	
	function handle($args) {
		parent::handle($args);
		$this->showPage();
	}

	function showContent() {

		if (Event::handle('StartShowPageHomeBlock', array(&$this))) {
			$this->elementStart('div',array('id'=>'home-block'));
			
			if (Event::handle('StartShowPageTitleBlock', array(&$this))) {
				$this->element('h2','welcome',sprintf(_('Hello %s'),$this->cur->fullname));
				Event::handle('EndShowPageTitleBlock', array(&$this));
			}
			
			$this->element('div',array('id'=>'form_file_upload_warning'),'');
			$this->apcUkey = ($this->apcEnable) ? common_good_rand(16) : false;

			if (Event::handle('StartShowPageUploadFormBlock', array(&$this))) {
				$form = new FileUploadForm($this,$this->apcUkey);
				$form->show();
				Event::handle('EndShowPageUploadFormBlock', array(&$this));
			}
			
			if (Event::handle('StartShowPageLoadingBlock', array(&$this))) {
				$this->elementStart('div',array('id'=>'form_file_upload_loading'));
				$this->elementStart('div');
				$this->element('img',array('src'=>'/images/loading.gif','alt'=>'Loading'));
				$this->elementEnd('div');
				$this->element('div',null,_('..please wait..'));
				$this->elementEnd('div');
				Event::handle('EndShowPageLoadingBlock', array(&$this));
			}

			$this->elementEnd('div');
			Event::handle('EndShowPageHomeBlock', array(&$this));
		}
	}
	
	function showScripts() {
		parent::showScripts();
		$this->script('jquery.iframe-post-form.js');
		if($this->apcEnable) {
            $this->inlineScript("FF.Util.Form.apcEnable = true;\nFF.Util.Form.apcUkey = '".$this->apcUkey."';");
		}
		$this->inlineScript("$(document).ready(function() {FF.Util.Form.Init();});");
	}
}