<?php
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2011, Fluidware
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class LoginAction extends FluidFrameAction {

	var $error;
	
	function title() {
		return _("Login");
	}
	
    function handle($args) {
        parent::handle($args);
        if (common_is_real_login()) {
        	$this->clientError(_('Already Logged.'));
        } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->checkLogin();
        } else {
            $this->showForm();
        }
    }
    
	function showContent() {
		$this->showPreviewForm();
	}
	
	/**
	* Show the appropriate form based on our input state.
	*/
	function showForm($err=null) {
		if ($err) {
			$this->error = $err;
		}
		if ($this->boolean('ajax')) {
			header('Content-Type: text/xml;charset=utf-8');
			$this->xw->startDocument('1.0', 'UTF-8');
			$this->elementStart('html');
			$this->elementStart('head');
			$this->element('title', null, $this->title());
			$this->elementEnd('head');
			$this->elementStart('body');
			$this->showContent();
			$this->elementEnd('body');
			$this->elementEnd('html');
		} else {
			$this->showPage();
		}
	}
	
	function showPreviewForm() {
	
		$this->elementStart('div', 'login_page_action');
		$this->element('h2',null,_('Welcome'));
		if (Event::handle('StartShowLoginPageForm', array($this))) {
			if($this->error) {
				$this->element('p','error',$this->error);
			} else {
				$this->element('p',null,'&nbsp;');
			}
			$this->elementStart('div',array('id'=>'login_page_form'));
			$this->elementStart('div',array('id'=>'login_block'));
			$this->elementStart('form', array('method' => 'post',
			                                          'id' => 'form_login',
			                                          'class' => 'form_settings',
			                                          'action' =>common_local_url('login')));
			
			$this->hidden('token', common_session_token());
			
			$this->elementStart('div');
			$this->elementStart('div','logininput');
			$this->input('email', _('Username'),'');
			$this->elementEnd('div');
			$this->elementStart('div','logininput');
			$this->password('password','Password');
			$this->elementEnd('div');
			$this->elementEnd('div');
			$this->elementStart('div');
			$this->submit('submit', _m('Login'), 'buttonmail', null,
			// TRANS: Button text.
			// TRANS: Tooltip for button "Login".
			_m('BUTTON','Login'));
			$this->elementEnd('div');
			$this->elementEnd('form');
			$this->elementEnd('div');
			$this->elementEnd('div');
			Event::handle('EndShowLoginPageForm', array($this));
		}
		$this->elementEnd('div');
	}
	
	function checkLogin($user_id=null, $token=null)
	{
		// XXX: login throttle
		if (Event::handle('StartCheckLogin', array($this))) {
			
			// CSRF protection - token set in NoticeForm
			$token = $this->trimmed('token');
			if (!$token || $token != common_session_token()) {
				$st = common_session_token();
				if (empty($token)) {
					common_log(LOG_WARNING, 'No token provided by client.');
				} else if (empty($st)) {
					common_log(LOG_WARNING, 'No session token stored.');
				} else {
					common_log(LOG_WARNING, 'Token = ' . $token . ' and session token = ' . $st);
				}

				$this->clientError(_('There was a problem with your session token. '.
	                                 'Try again, please.'));
				return;
			}

			$email = $this->trimmed('email');
			$password = $this->arg('password');

			$user = common_check_user($email, $password);

			if (!$user) {
				$this->showForm(_('Incorrect username or password.'));
				return;
			}

			// success!
			if (!common_set_user($user)) {
				$this->serverError(_('Error setting user. You are probably not authorized.'));
				return;
			}

			common_real_login(true);

			if ($this->boolean('rememberme')) {
				common_rememberme($user);
			}

			$url = common_get_returnto();

			if ($url) {
				// We don't have to return to it again
				common_set_returnto(null);
				$url = common_inject_session($url);
			} else {
				$url = common_local_url('home');
			}

			common_redirect($url, 303);
		}
	}
}