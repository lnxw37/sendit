<?php 
/**
* Fluidframe - Fluidware Web Framework
* Copyright (C) 2011, Fluidware
*
* @author: Michele Azzolari michele@fluidware.it
*
*/

if (!defined('FLUIDFRAME')) {
	exit(1);
}

class ActiveDirectoryAuthPlugin extends Plugin {
	
	var $adServers;
	var $adSuffix;
	var $adBaseDn;
	var $forcedEmailSuffix;
	
	function __construct($adServer=null,$adSuffix=null,$baseDn=null,$forceEmailSuffix=false) {
		parent::__construct();
		$this->adServers = $adServer;
		$this->adSuffix = $adSuffix;
		$this->adBaseDn = $baseDn;
		$this->forcedEmailSuffix=$forceEmailSuffix;
	}
	
	function onAutoload($cls) {
		switch ($cls) {
			case 'adLDAP':
				require_once(dirname(__FILE__) . '/adLDAP/adLDAP.php');
				return false;
		}
	}
	
	function onStartCheckPassword($username, $password, &$authenticatedUser) {
		
		if(!is_array($this->adServers)) {
			$this->adServers = array($this->adServers);
		}
		$options = array('base_dn'=>$this->adBaseDn,
			'account_suffix'=>$this->adSuffix,
			'domain_controllers'=>$this->adServers,
			'real_primarygroup'=>false
		);
		
		try {
			$adldap = new adLDAP($options);
			$auth = $adldap->authenticate($username, $password);
			if ($auth === TRUE) {
				$result=$adldap->user()->info($username);
				
				$authenticatedUser = new Profile();
				$authenticatedUser->id = common_good_rand(8);
				$authenticatedUser->fullname = $result[0]['displayname'][0];
				if($this->forcedEmailSuffix) {
					$authenticatedUser->email = $username .'@'.$this->forcedEmailSuffix;
				} else {
					$authenticatedUser->email =$result->email;
				}
				
				$authenticatedUser->username = $username;
				return FALSE;
			}
		} catch (adLDAPException $e) {
			common_log(LOG_ERR,$e->getMessage());
			return FALSE;
		}
		common_debug('Username: ' . $username . ' password non valida');
		
		return FALSE;
	}
	
}
?>