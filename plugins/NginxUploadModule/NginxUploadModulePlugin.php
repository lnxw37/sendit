<?php 
/**
* Fluidframe - Fluidware Web Framework
* Copyright (C) 2011, Fluidware
*
* @author: Michele Azzolari michele@fluidware.it
*
*/

if (!defined('FLUIDFRAME')) {
	exit(1);
}

/**
 * 
 * NginxUploadModule, it uses nginx upload module to handle uploaded files
 * 
 * 
 * @author macno
 *
 */
class NginxUploadModulePlugin extends Plugin {
	
	function onStartCheckFileUpload($action,$ok,$filename, $hash) {
		$tmp_name = $_POST["filename_path"];
		$name = $_POST["filename_name"];
		
		$filedir = common_config('site','upd-path');
		if(!file_exists($filedir))
		mkdir($filedir);
		$filepath = $filedir . '/' . $hash;
		
		$result = rename($tmp_name, $filepath);
		
		if (!$result) {
			common_debug('Failed to moved: from ' . $tmp_name . ' to ' . $filepath);
			// TRANS: Client exception thrown when a file upload operation fails because the file could
			// TRANS: not be moved from the temporary folder to the permanent file location.
			throw new ClientException(_('File could not be moved to destination directory.'));
			return;
		}
		common_debug('File moved: from ' . $tmp_name . ' to ' . $filepath);
		file_put_contents($filepath.'.info', $name);
		$ok = true;
		$filename = $name;
		return false;
	}
}