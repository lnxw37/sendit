<?php 
/**
* Fluidframe - Fluidware Web Framework
* Copyright (C) 2011, Fluidware
*
* @author: Michele Azzolari michele@fluidware.it
*
*/

if (!defined('FLUIDFRAME')) {
	exit(1);
}

/**
 * 
 * LoginFilter, it accepts login only from whitelistes IP classes
 * 
 * 
 * @author macno
 *
 */
class LoginFilterPlugin extends Plugin {
	
	var $allowedNETs = array();
	
	function __construct($allowedNETs=array()) {
		$this->allowedNETs = $allowedNETs;
		parent::__construct();
	}
	
	function onStartShowLoginPageForm($action) {
		return $this->_checkIP($action);
	}
	
	function onStartCheckLogin($action) {
		return $this->_checkIP($action);
	}
	
	function _checkIP($action) {
		$ip = $_SERVER['REMOTE_ADDR'];
		
		foreach ($this->allowedNETs as $net) {
			if($this->ip_in_range($ip,$net)) {
				common_debug('IP allowed in ' . $net . ' class');
				return true;
			}
		}
		$action->elementStart('div',array('id'=>'notallowed'));
		$action->element('span',null,_m('Login is forbidden from your location'));
		$action->elementEnd('div');
		common_debug('Access denied from ' . $ip);
		return false;
	}
	
	// decbin32
	// In order to simplify working with IP addresses (in binary) and their
	// netmasks, it is easier to ensure that the binary strings are padded
	// with zeros out to 32 characters - IP addresses are 32 bit numbers
	function decbin32 ($dec) {
		return str_pad(decbin($dec), 32, '0', STR_PAD_LEFT);
	}
	
	// ip_in_range
	// This function takes 2 arguments, an IP address and a "range" in several
	// different formats.
	// Network ranges can be specified as:
	// 1. Wildcard format:     1.2.3.*
	// 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
	// 3. Start-End IP format: 1.2.3.0-1.2.3.255
	// The function will return true if the supplied IP is within the range.
	// Note little validation is done on the range inputs - it expects you to
	// use one of the above 3 formats.
	function ip_in_range($ip, $range) {
		if (strpos($range, '/') !== false) {
			// $range is in IP/NETMASK format
			list($range, $netmask) = explode('/', $range, 2);
			if (strpos($netmask, '.') !== false) {
				// $netmask is a 255.255.0.0 format
				$netmask = str_replace('*', '0', $netmask);
				$netmask_dec = ip2long($netmask);
				return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
			} else {
				// $netmask is a CIDR size block
				// fix the range argument
				$x = explode('.', $range);
				while(count($x)<4) $x[] = '0';
				list($a,$b,$c,$d) = $x;
				$range = sprintf("%u.%u.%u.%u", empty($a)?'0':$a, empty($b)?'0':$b,empty($c)?'0':$c,empty($d)?'0':$d);
				$range_dec = ip2long($range);
				$ip_dec = ip2long($ip);
	
				# Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
				#$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));
	
				# Strategy 2 - Use math to create it
				$wildcard_dec = pow(2, (32-$netmask)) - 1;
				$netmask_dec = ~ $wildcard_dec;
	
				return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
			}
		} else {
			// range might be 255.255.*.* or 1.2.3.0-1.2.3.255
			if (strpos($range, '*') !==false) {
				// a.b.*.* format
				// Just convert to A-B format by setting * to 0 for A and 255 for B
				$lower = str_replace('*', '0', $range);
				$upper = str_replace('*', '255', $range);
				$range = "$lower-$upper";
			}
	
			if (strpos($range, '-')!==false) {
				// A-B format
				list($lower, $upper) = explode('-', $range, 2);
				$lower_dec = (float)sprintf("%u",ip2long($lower));
				$upper_dec = (float)sprintf("%u",ip2long($upper));
				$ip_dec = (float)sprintf("%u",ip2long($ip));
				return ( ($ip_dec>=$lower_dec) && ($ip_dec<=$upper_dec) );
			}
	
			echo 'Range argument is not in 1.2.3.4/24 or 1.2.3.4/255.255.255.0 format';
			return false;
		}
	
	}

}