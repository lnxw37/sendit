<?php 
/**
* Fluidframe - Fluidware Web Framework
* Copyright (C) 2011, Fluidware
*
* @author: Michele Azzolari michele@fluidware.it
*
*/

if (!defined('FLUIDFRAME')) {
	exit(1);
}

/**
 * 
 * LocalAuth, it needs a file with one user per line in this way:
 * 
 * username:md5 passwd:fullname:email
 * 
 * @author macno
 *
 */
class LocalAuthPlugin extends Plugin {
	
	var $passwd;
	
	function __construct($passwd=null) {
		$this->passwd = $passwd;
		parent::__construct();
	}
	
	function onStartCheckPassword($user, $password, &$authenticatedUser) {
		
		$lines = file($this->passwd,FILE_SKIP_EMPTY_LINES);
		if(!$lines) {
			common_debug('File: ' . $this->passwd . ' non trovato..');
			return FALSE;
		}
		foreach ($lines as $line) {
			$line = trim($line);
			$fields =  explode(':', $line);
			if($fields[0]==$user) {
				if($fields[1]==md5($password)) {
					$authenticatedUser = new Profile();
					$authenticatedUser->id = common_good_rand(8);
					$authenticatedUser->fullname = $fields[2];
					$authenticatedUser->email =$fields[3];
					$authenticatedUser->username = $fields[0];
					return FALSE;
				}
			}
		}
		return FALSE;
	}
}
?>