<?php 
/**
* Fluidframe - Fluidware Web Framework
* Copyright (C) 2011, Fluidware
*
* @author: Michele Azzolari michele@fluidware.it
*
*/

if (!defined('FLUIDFRAME')) {
	exit(1);
}

/**
 * 
 * InvitePlugin, let users send invite to an email to receive big files
 * 
 * 
 * @author macno
 *
 */
class InvitePlugin extends Plugin {
	
	function onAutoload($cls) {
	
		$dir = dirname(__FILE__);
	
		switch ($cls) {
			case 'SendinviteAction':
				include_once $dir . '/' . strtolower(mb_substr($cls, 0, -6)) . '.php';
				return false;
			case 'Page_file':
				include_once $dir.'/'.$cls.'.php';
				return false;
			case 'EditAttachFileForm':
				include_once $dir.'/'.strtolower($cls).'.php';
				return false;
			default:
				return true;
		}
	}
	
	function onRouterInitialized(Net_URL_Mapper $m) {
		$m->connect('main/invite/send',
		array('action' => 'sendinvite'));
		
		return true;
	}
	
	function onEndShowStyles($action) {
		$action->cssLink($this->path('invite.css'));
	}
	
	function onEndShowPageTitleBlock($action) {
		$action->elementStart('div',array('id'=>'ip-invite-block'));
		$action->element('a',array('href'=>common_local_url('sendinvite')),_('Send Invite'));
		$action->elementEnd('div');
	}
	
}