<?php 
/**
 * Fluidframe - Fluidware Web Framework
 * Copyright (C) 2012 Fluidware srl
 * 
 * @author: Michele Azzolari michele@fluidware.it
 * 
 */

$_startTime = microtime(true);
$_perfCounters = array();


define('INSTALLDIR', dirname(__FILE__));
define('FLUIDFRAME', true);

require_once INSTALLDIR . '/lib/common.php';


register_shutdown_function('common_log_perf_counters');

// Variabili globali
$user = null;
$action = null;

function getPath($req) {
    if ((common_config('site', 'fancy') || !array_key_exists('PATH_INFO', $_SERVER))
        && array_key_exists('p', $req)
    ) {
        return $req['p'];
    } else if (array_key_exists('PATH_INFO', $_SERVER)) {
        $path = $_SERVER['PATH_INFO'];
        $script = $_SERVER['SCRIPT_NAME'];
        if (substr($path, 0, mb_strlen($script)) == $script) {
            return substr($path, mb_strlen($script));
        } else {
            return $path;
        }
    } else {
        return null;
    }
}

function main() {
    global $user, $action;


    if (!_have_config()) {
        $msg = sprintf(
            _(
                "No configuration file found. Try running ".
                "the installation program first."
            )
        );
        $sac = new ServerErrorAction($msg);
        $sac->showPage();
        return;
    }
    
    $user = common_current_user();
    
    
    common_init_language();
    
    $path = getPath($_REQUEST);

    $r = Router::get();

    $args = $r->map($path);

    if (!$args) {
        $cac = new ClientErrorAction(_('Unknown page'), 404);
        $cac->showPage();
        return;
    }

    $args = array_merge($args, $_REQUEST);

    Event::handle('ArgsInitialize', array(&$args));

    $action = $args['action'];
    if (!$action || !preg_match('/^[a-zA-Z0-9_-]*$/', $action)) {
        common_redirect(common_local_url('public'));
        return;
    }
    
    $action_class = ucfirst($action).'Action';

    if (!class_exists($action_class)) {
        $cac = new ClientErrorAction(_('Unknown action'), 404);
        $cac->showPage();
    } else {
        $action_obj = new $action_class();

        try {
            if ($action_obj->prepare($args)) {
                $action_obj->handle($args);
            }
        } catch (ClientException $cex) {
            $cac = new ClientErrorAction($cex->getMessage(), $cex->getCode());
            $cac->showPage();
        } catch (ServerException $sex) { // snort snort guffaw
            $sac = new ServerErrorAction($sex->getMessage(), $sex->getCode(), $sex);
            $sac->showPage();
        } catch (Exception $ex) {
            $sac = new ServerErrorAction($ex->getMessage(), 500, $ex);
            $sac->showPage();
        }
    }
    
}


main();

// XXX: cleanup exit() calls or add an exit handler so
// this always gets called

Event::handle('CleanupPlugin');


?>